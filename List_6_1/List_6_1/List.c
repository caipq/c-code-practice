//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "List.h"
//
////创建结点
//ListNode* BuyListNode(LTDataType x)
//{
//	ListNode* node = (ListNode*)malloc(sizeof(ListNode));
//	node->next = NULL;
//	node->prev = NULL;
//	node->data = x;
//
//	return node;
//}
//
////初始化
////void ListInit(ListNode** pphead)
////{
////	*pphead = BuyListNode(0);
////	(*pphead)->next = *pphead;
////	(*pphead)->prev = *pphead;
////}
//
//ListNode* ListInit()
//{
//	ListNode* phead = BuyListNode(0);
//	phead->next = phead;
//	phead->prev = phead;
//
//	return phead;
//}
//
////打印
//void ListPrint(ListNode* phead)
//{
//	assert(phead);
//	ListNode* cur = phead->next;
//	while (cur != phead)
//	{
//		printf("%d ", cur->data);
//		cur = cur->next;
//	}
//	printf("\n");
//}
//
////尾插尾删
//void ListPushBack(ListNode* phead, LTDataType x)
//{
//	assert(phead);
//
//	ListNode* tail = phead->prev;
//	ListNode* newnode = BuyListNode(x);
//
//	tail->next = newnode;
//	newnode->prev = tail;
//
//	newnode->next = phead;
//	phead->prev = newnode;
//}
//
//void ListPopBack(ListNode* phead)
//{
//	assert(phead);
//	assert(phead->next != phead);
//
//	ListNode* tail = phead->prev;
//	ListNode* tailPrev = tail->prev;
//
//	tailPrev->next = phead;
//	phead->prev = tailPrev;
//	free(tail);
//	tail = NULL;
//}
//
////头插头删
//void ListPushFront(ListNode* phead, LTDataType x)
//{
//	assert(phead);
//
//	ListNode* first = phead->next;
//	ListNode* newnode = BuyListNode(x);
//
//	phead->next = newnode;
//	newnode->prev = phead;
//
//	newnode->next = first;
//	first->prev = newnode;
//}
//
//void ListPopFront(ListNode* phead)
//{
//	assert(phead);
//	assert(phead->next != phead);
//
//	ListNode* first = phead->next;
//	ListNode* second = first->next;
//
//	phead->next = second;
//	second->prev = phead;
//	free(first);
//}