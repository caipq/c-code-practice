//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//用栈实现队列
//请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：
//实现MyQueue类：
//void push(int x) 将元素x推到队列的末尾
//int pop() 从队列的开头移除并返回元素
//int peek() 返回队列开头的元素
//boolean empty() 如果队列为空，返回true；否则，返回false
//
//typedef struct 
//{
//    Stack _pushST;
//    Stack _popST;
//} MyQueue;
//
//
//MyQueue* myQueueCreate() {
//    MyQueue* q = (MyQueue*)malloc(sizeof(MyQueue));
//    StackInit(&q->_pushST);
//    StackInit(&q->_popST);
//
//    return q;
//}
//
//void myQueuePush(MyQueue* obj, int x) {
//    StackPush(&obj->_pushST, x);
//}
//
//int myQueuePop(MyQueue* obj) {
//    int front = myQueuePeek(obj);
//    StackPop(&obj->_popST);
//    return front;
//}
//
//int myQueuePeek(MyQueue* obj) {
//    if (!StackEmpty(&obj->_popST))
//    {
//        return StackTop(&obj->_popST);
//    }
//    else
//    {
//        while (!StackEmpty(&obj->_pushST))
//        {
//            StackPush(&obj->_popST, StackTop(&obj->_pushST));
//            StackPop(&obj->_pushST);
//        }
//    }
//
//    return StackTop(&obj->_popST);
//}
//
//bool myQueueEmpty(MyQueue* obj) {
//    return StackEmpty(&obj->_pushST) && StackEmpty(&obj->_popST);
//}
//
//void myQueueFree(MyQueue* obj) {
//    StackDestory(&obj->_pushST);
//    StackDestory(&obj->_popST);
//
//    free(obj);
//}

//设计循环队列。
//typedef struct 
//{
//    int* _a;
//    int _front;
//    int _rear;
//    int _k;
//} MyCircularQueue;
//
//
//MyCircularQueue* myCircularQueueCreate(int k) {
//    MyCircularQueue* q = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
//    q->_a = (int*)malloc(sizeof(int) * (k + 1));//多开一个
//    q->_front = 0;
//    q->_rear = 0;
//    q->_k = k;
//    return q;
//}
//
//bool myCircularQueueIsEmpty(MyCircularQueue* obj);
//bool myCircularQueueIsFull(MyCircularQueue* obj);
//
//bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
//    if (myCircularQueueIsFull(obj))
//        return false;
//
//    obj->_a[obj->_rear] = value;
//    obj->_rear++;
//    obj->_rear %= (obj->_k + 1);
//    return true;
//}
//
//bool myCircularQueueDeQueue(MyCircularQueue* obj) {
//    if (myCircularQueueIsEmpty(obj))
//        return false;
//
//    ++obj->_front;
//    obj->_front %= (obj->_k + 1);
//    return true;
//}
//
//int myCircularQueueFront(MyCircularQueue* obj) {
//    if (myCircularQueueIsEmpty(obj))
//        return -1;
//    else
//        return obj->_a[obj->_front];
//}
//
//int myCircularQueueRear(MyCircularQueue* obj) {
//    if (myCircularQueueIsEmpty(obj))
//    {
//        return -1;
//    }
//    else
//    {
//        int tail = obj->_rear - 1;
//        if (tail == -1)
//            tail = obj->_k;
//        return obj->_a[tail];
//    }
//}
//
//bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
//    return obj->_front == obj->_rear;
//}
//
//bool myCircularQueueIsFull(MyCircularQueue* obj) {
//    return (obj->_rear + 1) % (obj->_k + 1) == obj->_front;
//}
//
//void myCircularQueueFree(MyCircularQueue* obj) {
//    free(obj->_a);
//    free(obj);
//}