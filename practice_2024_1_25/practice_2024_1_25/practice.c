#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//
//int main()
//{
//	int arr[3][4];//3行4列的二维数组
//	char ch[3][10];
//
// 	int arr[3][4] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
//	int arr[3][4] = { {1,2,}, {3,4}, {5,6} };
//	int arr[][4] = { {1,2,}, {3,4}, {5,6} };//行可以省略；列不能省略
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 4; j++)
//		{
//			printf("%d ",arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	二维数组在数组中的存储
//	int arr[][4] = { {1,2,}, {3,4}, {5,6} };
//	int i = 0;
//	int j = 0;
//	int* p = &arr[0][0];
//
//	for (i = 0; i < 12; i++)
//	{
//		printf("%d ", *p);
//		p++;
//	}
//
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 4; j++)
//		{
//			printf("&arr[%d][%d] = %p\n", i, j, &arr[i][j]);
//		}
//		printf("\n");
//	}
//	
//	return 0;
//}


//void bubble_sort(int arr[], int sz)
//{
//	//确定趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//一趟冒泡排序的过程
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	//排序为升序 - 冒泡排序
//	//计算数组元素个数
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	bubble_sort(arr, sz);//数组传参的时候，传递的其实是数组首元素的地址
//
//	return 0;
//}

//数组名是什么
//数组名是首元素的地址
//但是有2个例外
//1.sizeof(数组名) - 数组名表示整个数组 - 计算的是整个数组的大小，单位是字节
//2.&数组名 - 数组名表示整个数组 - 取出的是整个数组的地址

//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", &arr);//&arr取出的是数组的地址
//	printf("%p\n", &arr+1);
//
//	printf("%p\n", arr);
//	printf("%p\n", arr+1);
//
//	printf("%p\n", &arr[0]);
//
//	int sz = sizeof(arr);//数组名表示整个数组
//	printf("%d\n", sz);
//
//	printf("%p\n", &arr[0]);
//	printf("%p\n", arr);//数组名是首元素的地址
//
//	return 0;
//}