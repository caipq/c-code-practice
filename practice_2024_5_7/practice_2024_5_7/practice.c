#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.dat", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//写文件 - 按照行来写
//	fputs("abcdef\n", pf);//将字符串"abcdef"写到pf指向的文件流中去
//	fputs("qwerty\n", pf);//将字符串"qwerty"写到pf指向的文件流中去
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//int main()
//{
//	char arr[10] = { 0 };
//	//打开文件
//	FILE* pf = fopen("test.dat", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	fgets(arr, 4, pf);
//	printf("%s\n", arr);
//
//	fgets(arr, 4, pf);
//	printf("%s\n", arr);
//	//从pf指向的文件流中读取4个字符，放到arr中
//	//实际只读取3个字符，最后一个为'\0'
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//struct S
//{
//	char arr[10];
//	int num;
//	float sc;
//};
//
//int main()
//{
//	struct S s = { "abcdef", 10, 5.5f };
//	
//	//对格式化的数据进行写文件
//	FILE* pf = fopen("test.dat", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	fprintf(pf, "%s %d %f\n", s.arr, s.num, s.sc);
//	//将结构体s中的数据以%s、%d和%f的形式写入pf指向的文件流中
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//struct S
//{
//	char arr[10];
//	int num;
//	float sc;
//};
//
//int main()
//{
//	struct S s = { 0 };
//
//	//对格式化的数据进行读文件
//	FILE* pf = fopen("test.dat", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	fscanf(pf, "%s %d %f", s.arr, &(s.num), &(s.sc));
//	//将pf指向的文件流中的数据读取到结构体s中
//
//	//打印
//	printf("%s %d %f\n", s.arr, s.num, s.sc);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//struct S
//{
//	char arr[10];
//	int num;
//	float sc;
//};
//
//int main()
//{
//	struct S s = { "abcdef", 10, 5.5f };
//
//	//以二进制的形式写文件
//	FILE* pf = fopen("test.dat", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//写文件
//	fwrite(&s, sizeof(struct S), 1, pf);
//	//将结构体s中的大小为struct S的数据写到pf指向的文件流中，总共写1个
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//struct S
//{
//	char arr[10];
//	int num;
//	float sc;
//};
//
//int main()
//{
//	struct S s = { 0 };
//
//	//以二进制的形式读文件
//	FILE* pf = fopen("test.dat", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	fread(&s, sizeof(struct S), 1, pf);
//	//读取pf指向的文件流中的1个大小为struct S的数据，放到结构体s中
//
//	//打印
//	printf("%s %d %f\n", s.arr, s.num, s.sc);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}