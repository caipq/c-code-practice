#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//
//struct S
//{
//	char arr[10];
//	int age;
//	float f;
//};
//
//int main()
//{
//	struct S s = { "hello", 20, 5.5f };
//	struct S tmp = { 0 };
//	char buf[100] = { 0 };
//
//	//sprintf - 把一个格式化的数据转换成字符串
//	sprintf(buf, "%s %d %f", s.arr, s.age, s.f);
//	printf("%s\n", buf);
//
//	//sscanf - 从buf字符串中还原出一个结构体数据
//	sscanf(buf, "%s %d %f", tmp.arr, &(tmp.age), &(tmp.f));
//	printf("%s %d %f\n", tmp.arr, tmp.age, tmp.f);
//
//	return 0;
//}
//
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	//假设文件的内容为"abcdef"
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	//调整文件指针
//	fseek(pf, 2, SEEK_CUR);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//d
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//e
//
//	int ret = ftell(pf);
//	printf("%d\n", ret);//5
//
//	//让文件指针回到起始位置
//	rewind(pf);
//	ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//写代码把test.txt文件拷贝一份，生成test2.txt
//
//int main()
//{
//	FILE* pfread = fopen("test.txt", "r");
//	if (pfread == NULL)
//	{
//		return 1;
//	}
//	FILE* pfwrite = fopen("test2.txt", "w");
//	if (pfwrite == NULL)
//	{
//		fclose(pfread);
//		pfread = NULL;
//		return 1;
//	}
//	//文件打开成功
//	//读写文件
//	int ch = 0;
//	while ((ch = fgetc(pfread)) != EOF)
//	{
//		写文件
//		fputc(ch, pfwrite);
//	}
//
//	if (feof(pfread))
//	{
//		pritnf("遇到文件结束标志，文件正常结束\n");
//	}
//	else if (ferror(pfread))
//	{
//		printf("文件读取失败结束\n");
//	}
//
//	//关闭文件
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//
//	return 0;
//}