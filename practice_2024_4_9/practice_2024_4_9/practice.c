//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>
//#include<stdlib.h>

//int main()
//{
//	char* p = "1234";
//	int ret = atoi(p);
//	printf("%d\n%", ret);
//
//	return 0;
//}

//模拟实现stoi
//
//#include<ctype.h>
//#include<limits.h>
//enum State
//{
//	INVALID,//0
//	VALID//1
//};

//state 记录的是my_atoi返回的值是合法转化的值，还是非法的状态
//enum State state = INVALID;
//
//int my_atoi(const char* s)
//{
//	int flag = 1;
//	//assert(s);
//	if (NULL == s)
//	{
//		return 0;
//	}
//	//空字符串
//	if (*s == '\0')
//	{
//		return 0;
//	}
//	//跳过空白字符
//	while (isspace(*s))
//	{
//		s++;
//	}
//	//+/-
//	if (*s == '+')
//	{
//		flag = 1;
//		s++;
//	}
//	else if (*s == '-')
//	{
//		flag = -1;
//		s++;
//	}
//	//处理数字字符
//	long long n = 0;
//	while (isdigit(*s))
//	{
//		n = n * 10 + flag * (*s - '0');
//		if (n > INT_MAX || n < INT_MIN)
//		{
//			return 0;
//		}
//		s++;
//	}
//	if (*s == '\0')
//	{
//		state = VALID;
//		return (int)n;
//	}
//	else
//	{
//		//非数字字符
//		return (int)n;
//	}
//}
//
//int main()
//{
//	//1.空指针
//	//2.空字符串
//	//3.遇到非数字字符
//	//4.超出范围
//	const char* p = "1234";
//	int ret = my_atoi(p);
//	if (state == VALID)
//		printf("正常的转换:%d\n", ret);
//	else
//		printf("非法的转换:%d\n", ret);
//	  
//	return 0;
//}

//找单身狗
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次
//编写一个函数找出这两个只出现一次的数字
//
//void Find(int arr[], int sz, int* px, int* py)
//{
//	//1.把所有数字异或
//	int i = 0;
//	int ret = 0;
//	for (i = 0; i < sz; i++)
//	{
//		ret ^= arr[i];
//	}
//	//2.计算ret的哪一位为1
//	//ret = 3
//	//011
//	int pos = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((ret >> i) & 1) == 1)
//		{
//			pos = i;
//			break;
//		}
//	}
//	//把从低位向高位的第pos位为1的放在一个组，为0的放在另外一个组
//	int num1 = 0;
//	int num2 = 0;
//	for (i = 0; i < sz; i++)
//	{
//		if (((arr[i] >> pos & 1) == 1))
//		{
//			num1 ^= arr[i];
//		}
//		else
//		{
//			num2 ^= arr[i];
//		}
//	}
//	*px = num1;
//	*py = num2;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,1,2,3,4 };
//	//1 3 1 3 5
//	//2 4 2 4 6
//	//1.分组
//	//2.分组的要点：5和6必须在不同的组
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int x = 0;
//	int y = 0;
//	Find(arr, sz, &x, &y);
//	printf("%d %d\n", x, y);
//	return 0;
//}