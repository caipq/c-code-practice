//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<iostream>
//using namespace std;
//
//int main()
//{
//	//权限缩小和放大规则：适用于引用和指针间
//	//权限放大 const不能给非const，const只能给const
//	const int a = 10;
//	//int& b = a;//不行
//	const int& b = a;
//
//	//权限缩小 非const既可以给非const，也可以给const
//	int c = 20;
//	int& d = c;
//	const int& e = c;
//
//	const int* cp1 = &a;
//	//int* p1 = cp1;//不能，属于权限的放大
//	const int* p1 = cp1;
//
//	int* p2 = &c;
//	const int* cp2 = p2;//可以，属于权限的缩小
//
//	//下面可以，不跟上面的规则
//	const int x = 10;
//	int y = x;
//
//	int z = 20;
//	const int r = z;
//
//	return 0;
//}

//引用的场景
//1.引用做参数
//a.输出型参数
//b.提高效率
//void swap_c(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
//void swap_cpp(int& r1, int& r2)
//{
//	int tmp = r1;
//	r1 = r2;
//	r2 = tmp;
//}
//
//int main()
//{
//	int a = 0, b = 1;
//	swap_c(&a, &b);
//	swap_cpp(a, b);
//
//	return 0;
//}

//2.引用做返回值
//a.提高效率
//b.以后再讲
//int Count1()
//{
//	static int n = 0;
//	n++;
//
//	return n;
//}
//
//int& Count2()
//{
//	static int n = 0;
//	n++;
//
//	return n;
//}
//
//int main()
//{
//	const int& r1 = Count1();
//	int& r2 = Count2();
//
//	return 0;
//}
//
//int& Add(int a, int b)
//{
//	//int c = a + b;
//	static int c = a + b;
//
//	return c;
//}
//int main()
//{
//	int& ret = Add(1, 2);
//	Add(3, 4);
//	cout << "Add(1, 2) is :" << ret << endl;
//	return 0;
//}
//
//#include <time.h>
//struct A 
//{ 
//	int a[10000]; 
//};
//A a;
//A TestFunc1() 
//{
//	return a;
//}
//A& TestFunc2() 
//{
//	return a;
//}
//void main()
//{
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//
//	int a = 10;
//	int& b = a;
//
//	int* p = &a;
//
//	return 0;
//}

//内联函数
//inline void Swap(int& x1, int& x2)
//{
//	int tmp = x1;
//	x1 = x2;
//	x2 = tmp;
//}
//
//int main()
//{
//	int a = 0, b = 2;
//	Swap(a, b);
//
//	return 0;
//}
//
//int main()
//{
//	int a = 0;
//	auto b = a;//b的类型是根据a的类型推导出是int
//	int& c = a;
//
//	auto& d = a;
//	auto* e = &a;
//	auto f = &a;
//
//	cout << typeid(a).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//
//	cout << typeid(d).name() << endl;
//	cout << typeid(e).name() << endl;
//	cout << typeid(f).name() << endl;
//
//	return 0;
//}
//
//int main()
//{
//	int array[] = { 1,2,3,4,5 };
//	//要求将数组中的值乘2倍，再打印一遍
//
//	//C语言中的做法
//	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
//	{
//		array[i] *= 2;
//	}
//
//	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
//	{
//		cout << array[i] << " ";
//	}
//	cout << endl;
//
//	//C++11 ->范围for(语法糖) -》 特点：写起来比较简洁
//	for (auto e : array)
//	{
//		e *= 2;
//	}
//
//	for (auto e : array)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//
//	return 0;
//}
//
//void fun(int n)
//{
//	cout << "整型" << endl;
//}
//
//void fun(int* p)
//{
//	cout << "整型" << endl;
//}
//
//int main()
//{
//	//C
//	int* p1 = NULL;
//
//	//C++11中，推荐像下面这样去用
//	int* p2 = nullptr;
//
//	fun(0);
//	fun(NULL);//预处理后变成fun(0)
//	fun(nullptr);//fun((void*)0)
//
//	return 0;
//}

//类里面可以定义：
//1.成员变量
//2.成员函数
//class Person
//{
//	void Print()
//	{
//
//	}
//
//	char _name[10];
//	int _age;
//	//...
//};
//
//class Student
//{
//public:
//	void ShowInfo()
//	{
//		cout << _name << endl;
//		cout << _age << endl;
//		cout << _stuid << endl;
//	}
//
//	int GetAge()
//	{
//		return _age;
//	}
//
//private:     //一般情况下成员变量都是比较隐私的，都会定义成私有或保护
//	char _name[20];
//	int _age;
//	int _stuid;
//	//...
//};
//
//int main()
//{
//	Student s1;
//	Student s2;
//	//s1._age = 10;//无法访问私有的
//
//	return 0;
//}

//定义一个数据结构栈的类
//class Stack
//{
//public:
//	void Init(size_t n)
//	{
//		//...
//	}
//
//	void Destory()
//	{
//		//...
//	}
//
//	void Push(int x)
//	{
//		//...
//	}
//
//	void Pop()
//	{
//		//...
//	}
//
//	//...
//	
////protected:
//private:
//	int* _a;
//	size_t _top;
//	size_t capacity;
//};
//
//int main()
//{
//	Stack st;
//	st.Init(10);
//	st.Push(1);
//	st.Push(2);
//	st.Push(3);
//
//	st.Pop();
//
//	st.Destory();
//
//	return 0;
//}

//1.C语言中struct是用来定义结构体的
//2.C++中，兼容C的struct定义结构体的用法，但是同时struct也可以用来定义类
//3.C++中使用class和struct定义类的区别：默认的访问限定符
//
//C
//struct ListNode_C
//{
//	int _val;
//	struct ListNode_C* _next;
//	struct ListNode_C* _prev;
//};
//
////C++
////struct ListNode_CPP
//class ListNode_CPP //class默认是私有的
//{
//	int _val;
//	struct ListNode_CPP* _next; //兼容C的用法
//	ListNode_CPP* _prev;        //C++当成类的用法
//
//	//还可以定义函数->成员函数
//	ListNode_CPP* CreateNode(int val);
//};
//
////1.声明和定义的区别
////声明是一种承诺，承诺要干嘛，但是还没做，定义就是把这个事情落地了
////如何定义一个类
////封装
////1.将数据和方法放到定义一起
////2.把想给你看到的数据给你看，不想给你看的封装起来 --> 访问限定符
//class Stack
//{
//public:
//	//1.成员函数
//	void Push(int x)  //在类里面定义
//	{
//		//...
//	}
//	void Pop();  //在类里面声明，类外面定义
//
//	bool Empty(); //这里是声明
//	//...
//
//private:
//	//2.成员变量
//	int* _a;
//	int _size;
//	int _capacity; //这里是声明
//};
//
////类外面定义
//void Stack::Pop()
//{
//	//...
//}
//
//// 类中既有成员变量，又有成员函数
//class A1
//{
//public:
//	void f1()
//	{}
//private:
//	int _a;
//	char _ch;
//};//大小为4
//
//// 类中仅有成员函数
//class A2
//{
//public:
//	void f2()
//	{}
//};//大小为1
//
//// 类中什么都没有---空类
//class A3
//{};//大小为1
//
//int main()
//{
//	//类实例化出对象，相当于定义出了类的成员变量
//	Stack s1;
//	Stack s2;
//	Stack s3;
//	s1.Push(1);
//
//	//对象中只存储成员变量，不存储成员函数
//	//一个类实例化出N个对象，每个对象的成员变量都可以存储不同的值，但是调用的函数却是同一个
//	//如果每个对象都放成员函数，而这些成员函数却是一样的，浪费空间
//	cout << sizeof(s1) << endl;
//
//	//如何计算一个类实例化出的对象的代销，计算成员变量之和，并且考虑内存对齐规则
//	//没有成员变量类的大小是1，为什么是1，而不是0？开一个字节不是为了存数据，而是占位，表示对象存在
//
//	return 0;
//}

//实例化 -> 计算用自己定义的类型定义出对象
//1.内置类型 - 基本类型 int/char/double
//2.自定义类型 class/struct
//
//class Date
//{
//public:
//	//this是调用这个成员函数，this就指向谁
//	void Init(int year, int month, int day) //void Init(Date* this, int year, int month, int day)
//	{
//		_year = year; //this->_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print() //void Print(Date* this)
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//this指针存在哪里？(也就是存在进程地址空间的哪个区域？)
//答：栈上的，因为它是一个形参。（ps：VS下是在ecx这个寄存器，来传递）
//
//int main()
//{
//	Date d1;
//	d1.Init(2024, 7, 11);//d1.Init(&d1, 2024, 7, 11);
//	d1.Print();//d1.Print(&d1);
//
//	return 0;
//}
//
//class A
//{
//public:
//	void PrintA()
//	{
//		cout << _a << endl;
//	}
//
//	void Show()
//	{
//		cout << "Show()" << endl;
//	}
//
//private:
//	int _a;
//};
//
//int main()
//{
//	A* p = NULL;
//	p->PrintA();//程序崩溃
//	p->Show();//正常运行
//	//成员函数存在公共的代码段，所以p->Show()这里不会去p指向的对象上找
//	//访问成员函数，才回去找
//
//	return 0;
//}

//构造函数
//1. 函数名与类名相同。
//2. 无返回值。
//3. 对象实例化时编译器自动调用对应的构造函数。
//4. 构造函数可以重载。
//
//class Date
//{
//public:
//	//构造函数 ->在对象构造时调用的函数，这个函数完成初始化工作
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	Date()
//	{
//		_year = 0;
//		_month = 1;
//		_day = 1;
//	}
//
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	//对象实例化时自动调用
//	Date d1(2024, 7, 11);
//	d1.Print();
//
//	Date d2; // Date d2() - 不能加括号
//	d2.Print();
//
//	return 0;
//}

//5. 如果类中没有显式定义构造函数，则C++编译器会自动生成一个无参的默认构造函数，
//一旦用户显式定义编译器将不再生成。
//class Time
//{
//public:
//	Time()
//	{
//		_hour = 0;
//		_minute = 0;
//		_second = 0;
//
//		cout << "Time()" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};

//class Date
//{
//public:
//	//我们没有显式定义构造函数，这里编译器会生成无参的默认构造函数
//	//默认生成无参构造函数(语法坑：双标)
//	//1.针对内置类型的成员变量没有做处理
//	//2.针对自定义类型的成员变量，调用它的构造函数初始化
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//
//	Time _t;
//};
//
//int main()
//{
//	Date d1;//调用编译器生成的构造函数
//	d1.Print();
//
//	return 0;
//}

//7.无参的构造函数和全缺省的构造函数都称为默认构造函数，并且默认构造函数只能有一个。
//注意：无参构造函数、全缺省构造函数、我们没写编译器默认生成的构造函数，都可以认为
//是默认构造函数。
//class Date
//{
//public:
//	//一旦用户显式定义编译器将不再生成
//	//Date()
//	//{
//	//	_year = 0;
//	//	_month = 1;
//	//	_day = 1;
//	//}
//
//	//Date(int year, int month, int day)
//	//{
//	//	_year = year;
//	//	_month = month;
//	//	_day = day;
//	//}
//
//	//更好的方式->全缺省
//	Date(int year = 0, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1;//调用默认构造函数->1.自己实现无参的构造函数 2.自己实现的全缺省构造函数 3.我们不写，编译器自动生成的构造函数
//	                            //特点：不用传参数
//	d1.Print();
//
//	Date d2(2024, 7, 11);
//	d2.Print();
//
//	return 0;
//}
//
//class Date
//{
//public:
//	Date(int year = 0, int month = 1, int day = 1)
//	{
//		cout << "Date(int year = 0, int month = 1, int day = 1)" << endl;
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//	//析构函数
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//class Stack
//{
//public:
//	Stack(int n = 10)
//	{
//		_a = (int*)malloc(sizeof(int) * n);
//		_size = 0;
//		_capacity = n;
//	}
//
//	~Stack()
//	{
//		if (_a)
//		{
//			free(_a);
//			cout << "free:" <<_a << endl;
//			_a = nullptr;
//			_size = _capacity = 0;
//		}
//	}
//
//private:
//	int* _a;
//	int _size;
//	int _capacity;
//};
//
//int main()
//{
//	//析构：对象生命周期到了以后自动调用，完成对象里面的资源清理工作，不是完成d1和d2的销毁
//	Date d1;
//	Date d2;
//
//	Stack s1;
//	Stack s2;
//
//	//...Push
//
//	return 0;
//}