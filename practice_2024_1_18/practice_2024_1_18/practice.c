#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "hello bit";
//	strcpy(arr1,arr2);
//	printf("%s",arr1);//打印arr1这个字符串 %s - 以字符串的格式来打印
//
//	return 0;
//}

//int main()
//{
//	char arr[] = "hello bit";
//	memset(arr, 'x', 5);//把arr的前5个字符设置成x
//
//	printf("%s\n", arr);
//
//	return 0;
//}

//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "abc";
//
//	strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//
//	return 0;
//}

//写一个函数可以找出两个整数中的最大值
// 
//int get_max(int x, int y)
//{
//	int z = 0;
//	if (x > y)
//		z = x;
//	else
//		z = y;
//	return z;//返回z-返回较大值
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	//函数的调用
//	int max = get_max(a, b);
//  int max = get_max(2+3, 7);
//  int max = get_max(2+3, get_max(4,7));
//	printf("max = %d\n", max);
//
//	return 0;
//}

//写一个函数可以交换两个整型变量的内容

//函数返回类型的地方写成void，表示这个函数不返回任何值，也不需要返回
//写出问题了！
//void Swap(int x, int y)
//{
//	int z = 0;
//	z = x;
//	x = y;
//	y = z;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//
//	printf("交换前：a=%d b=%d\n", a, b);
//	Swap(a, b);
//	printf("交换后：a=%d b=%d\n", a, b);
//
//	return 0;
//}

//int main()
//{
//	int a = 10;//4个字节的空间
//
//	int* pa = &a;//pa就是一个指针变量
//	*pa = 20;
//
//	printf("%d\n", a);
//
//	return 0;
//}

//Swap1在被调用的时候，实参传给形参，其实形参是实参的一份临时拷贝
//改变形参，不能改变实参
//void Swap1(int x, int y)  //形式参数 - 形参  
//{
//	int z = 0;
//	z = x;
//	x = y;
//	y = z;
//}
// 
//void Swap2(int* pa, int* pb) //形式参数 - 形参  
//{  
//	int z = 0;
//	z = *pa;
//	*pa = *pb;
//	*pb = z;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//
//	Swap1(a, b); //实际参数 - 实参    传值调用
//	printf("交换前：a=%d b=%d\n", a, b);
//	Swap2(&a, &b); //实际参数 - 实参  传址调用
//	printf("交换后：a=%d b=%d\n", a, b);
//
//	return 0;
//}

//写一个函数可以判断一个数是不是素数

//#include<math.h>
//
//int is_prime(int n)
//{
//	//2 -> n-1 之间的数字
//	int j = 0;
//	for (j = 2; j <= sqrt(n); j++)
//	{
//		if (n % j == 0)
//			return 0;
//	}
//
//	return 1;
//}
//
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i) == 1)
//		{
//			count++;
//			printf("%d ", i);
//		}
//	}
//
//	printf("\ncount = %d\n", count);
//
//	return 0;
//}

//写一个函数判断一年是不是闰年

//is_leap_year
//如果判断是闰年返回1
//不是闰年，返回0

// 一个函数如果不写返回类型，默认返回int类型

//int is_leap_year(int n)
//{
//	if (((n % 4 == 0) && (n % 100 != 0)) || (n % 400 == 0))
//		return 1;
//	else
//		return 0;
//}

int is_leap_year(int n)
{
	return (((n % 4 == 0) && (n % 100 != 0)) || (n % 400 == 0));

}

//int main()
//{
//	int y = 0;
//	int count = 0;
//	for (y = 1000; y <= 2000; y++)
//	{
//		if (is_leap_year(y) == 1)
//		{
//			count++;
//			printf("%d ", y);
//		}
//	}
//	printf("\ncount = %d\n", count);
//
//	return 0;
//}

//写一个函数，实现一个整型有序数组的二分查找

//int binary_search(int a[], int k, int s)
//{
//	int left = 0;
//	int right = s - 1;
//
//	while (left <= right)
//	{
//		int mid = (left + right) / 2;
//		if (a[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else if (a[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;//找不到了
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int key = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	//找到了就返回找到位置的下标
//	//找不到返回-1
//	//数组arr传参，实际传递的不是数组本身
//	//仅仅传过去了数组首元素的地址
//	int ret = binary_search(arr, key, sz);
//
//	if (-1 == ret)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("找到了，下标是：%d\n", ret);
//	}
//	return 0;
//}