//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>


//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和


//int DigitSum(int n)
//{
//	if (n > 9)
//	{
//		return DigitSum(n / 10) + n % 10;
//	}
//	else
//		return n;
//}
//
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int sum = DigitSum(num);
//
//	printf("%d\n", sum);
//	return 0;
//}


//编写一个函数实现n的k次方，使用递归实现

//double Pow(int n, int k)
//{
//	if (k == 0)
//		return 1;
//	else if (k > 0)
//		return n * Pow(n, k - 1);
//	else
//		return 1.0 / (Pow(n, -k));
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	double ret = Pow(n, k);
//	printf("%lf\n", ret);
//
//	return 0;
//}

//#include<string.h>
//
//int main()
//{
//	char ch1[] = "bit";
//	char ch2[] = { 'b','i','t' };
//
//	printf("%s\n", ch1);
//	printf("%s\n", ch2);
//
//	printf("%d\n", strlen(ch1));
//	printf("%d\n", strlen(ch2));
//
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };//不完全初始化，第一个为0，其他默认为0
//	arr[4] = 5;
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	
//	return 0;
//}

//%p - 是按照地址的格式打印 - 十六进制的打印
//int main()
//{
//	//printf("%x\n", 0x12);
//	//printf("%p\n", 0x12);
//
//	int arr[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("&arr[%d] = %p\n", i, &arr[i]);
//	}
//
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;//数组名是数组首元素的地址
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ",*p);
//		p++;
//	}
//
//	return 0;
//}