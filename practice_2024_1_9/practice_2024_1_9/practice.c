#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int main()
//{
//	int a = 10;
//	printf("%p\n", &a);//%p 专门用来打印地址的
//	int * pa = &a;//pa是用来存放地址的，在C语言中pa叫指针变量
//
//	//* 说明pa 是指针变量
//	//int 说明pa执行的对象是int类型的
//
//	char ch = 'w';
//	char* pc = &ch;
//
//	return 0;
//}

//int main()
//{
//	int* pa = &pa;
//
//	return 0;
//}

//指针 就是 地址
//int main()
//{
//	int a = 10;
//
//	int* pa = &a;
//
//	*pa = 20;//* 解引用操作，*pa就是通过pa里边的地址找到a
//
//	printf("%d\n", a);
//
//	return 0;
//}

//int main()
//{
//	printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(short*));
//	printf("%d\n", sizeof(int*));
//	printf("%d\n", sizeof(long*));
//	printf("%d\n", sizeof(long long*));
//	printf("%d\n", sizeof(float*));
//	printf("%d\n", sizeof(double*));
//	
//	return 0;
//}

////创建一个学生的类型
//struct Stu
//{
//	char name[20];
//	int age;
//	double score;
//};
//
////创建一个书的类型
//struct Book
//{
//	char name[20];
//	float price;
//	char id[30];
//};

//int main()
//{
//	struct Stu s = { "张三",20,85.5 };//结构体的创建和舒适化
//	printf("1: %s %d %lf\n", s.name, s.age, s.score);//结构体变量。成员变量
//	
//	struct Stu* ps = &s;
//	printf("2: %s %d %lf\n", (*ps).name, (*ps).age, (*ps).score);
//
//	printf("3: %s %d %lf\n", ps->name, ps->age, ps->score);
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int b = 20;
//	int max = 0;
//	max = a > b ? a : b;
//
//	printf("%d\n", max);
//
//	return 0;
//}

//int Max(int x, int y)
//{
//	if (x > y)
//		return x;
//	else
//		return y;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//
//	int m = Max(a, b);
//	printf("%d\n", m);
//
//	return 0;
//}

//int main()
//{
//	printf("     **     \n");
//	printf("************\n");
//	printf("************\n");
//	printf("    *  *    \n");
//	printf("    *  *    \n");
//	return 0;
// }

//int main()
//{
//	int m = 0;
//	scanf("%d", &m);
//	if (m % 5 == 0)
//		printf("YES\n");
//	else
//		printf("NO\n");
//
//	return 0;
//}