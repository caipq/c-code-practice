//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

////反转一个单链表
//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* newhead = NULL;
//    struct ListNode* cur = head;
//
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//
//        //头插
//        cur->next = newhead;
//        newhead = cur;
//
//        cur = next;
//    }
//
//    return newhead;
//}

////删除链表中等于给定值 val 的所有结点
//typedef struct ListNode Node;
//struct ListNode* removeElements(struct ListNode* head, int val) {
//    Node* prev = NULL, * cur = head;
//    while (cur)
//    {
//        if (cur->val == val)
//        {
//            //if(prev == NULL)
//            if (cur == head)
//            {
//                head = cur->next;
//                free(cur);
//                cur = head;
//            }
//            else
//            {
//                prev->next = cur->next;
//                free(cur);
//                cur = prev->next;
//            }
//        }
//        else
//        {
//            prev = cur;
//            cur = cur->next;
//        }
//    }
//
//    return head;
//}

//给定一个带有头结点 head 的非空单链表，返回链表的中间结点。如果有两个中间结点，
//则返回第二个中间结点
//快慢指针
//struct ListNode* middleNode(struct ListNode* head) {
//    struct ListNode* slow = head;
//    struct ListNode* fast = head;
//
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//    }
//
//    return slow;
//}

//输入一个链表，输出该链表中倒数第k个结点
//typedef struct ListNode Node;
//struct ListNode* FindKthTaTail(struct ListNode* pListHead, int k)
//{
//	Node* slow = pListHead;
//	Node* fast = pListHead;
//
//	while (k--)
//	{
//		if (fast)
//		{
//			fast = fast->next;
//		}
//		else
//		{
//			return NULL;
//		}
//	}
//
//	while (fast)
//	{
//		slow = slow->next;
//		fast = fast->next;
//	}
//
//	return slow;
//}