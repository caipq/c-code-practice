//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//编写代码，以给定值x为基准将链表分割成两部分，所有小于x的结点排在大于或等于x的结点之前
/*
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
//#include <cstddef>
//#include <exception>
//class Partition {
//public:
//    ListNode* partition(ListNode* pHead, int x) {
//        // write code here
//        ListNode* lessHead, * lessTail;
//        ListNode* greaterHead, * greaterTail;
//        lessHead = lessTail = (ListNode*)malloc(sizeof(ListNode));
//        greaterHead = greaterTail = (ListNode*)malloc(sizeof(ListNode));
//        lessHead->next = greaterHead->next = NULL;
//
//        ListNode* cur = pHead;
//        while (cur)
//        {
//            if (cur->val < x)
//            {
//                lessTail->next = cur;
//                lessTail = lessTail->next;
//            }
//            else
//            {
//                greaterTail->next = cur;
//                greaterTail = greaterTail->next;
//            }
//
//            cur = cur->next;
//        }
//
//        lessTail->next = greaterHead->next;
//        greaterTail->next = NULL;
//
//        ListNode* list = lessHead->next;
//        free(lessHead);
//        free(greaterHead);
//
//        return list;
//    }
//}

//链表的回文结构
/*
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* newhead = NULL;
//    struct ListNode* cur = head;
//
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//
//        //头插
//        cur->next = newhead;
//        newhead = cur;
//
//        cur = next;
//    }
//
//    return newhead;
//}
//class PalindromeList {
//public:
//    bool chkPalindrome(ListNode* A) {
//        // write code here
//        ListNode* fast = A;
//        ListNode* slow = A;
//        ListNode* prev = NULL;
//
//        while (fast && fast->next)
//        {
//            prev = slow;
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//
//        prev->next = NULL;
//
//        slow = reverseList(slow);
//
//        while (A)
//        {
//            if (A->val != slow->val)
//            {
//                return false;
//            }
//            else
//            {
//                A = A->next;
//                slow = slow->next;
//            }
//        }
//
//        return true;
//    }
//}