//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//struct Book
//{
//	char name[20];
//	int price;
//	char id[12];
//}b4, b5, b6;//b4,b5,b6是全局变量
//
//int main()
//{
//	//b1,b2,b3是局部变量
//	struct Book b1;
//	struct Book b2;
//	struct Book b3;
//
//	return 0;
//}

//匿名结构体类型,只能用一次
//struct
//{
//	char c;
//	int i;
//	char ch;
//	double d;
//}s;
//
//struct
//{
//	char c;
//	int i;
//	char ch;
//	double d;
//}* ps;
//
//struct A
//{
//	int i;
//	char c;
//};
//
//struct B
//{
//	char c;
//	struct A sa;
//	double d;
//};
//
//struct Node
//{
//	int data;
//	struct Node* next;//字节大小:4/8
//};
//
//struct S
//{
//	char c;
//	int i;
//}s1, s2;
//
//struct B
//{
//	double d;
//	struct S s;
//	char c;
//};
//
//int main()
//{
//	struct S s3 = { 'x', 20 };
//	struct B sb = { 3.14, {'w', 100}, 'q' };
//	//结构体变量.
//	//结构体指针->
//	printf("%lf %c %d %c\n", sb.d, sb.s.c, sb.s.i, sb.c);
//
//	return 0;
//}

//struct S
//{
//	int i;
//	char c;
//};
//
////结构体内存对齐
//
//int main()
//{
//	struct S s = { 0 };
//	printf("%d\n", sizeof(s));
//
//	return 0;
//}