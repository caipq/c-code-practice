//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>
//#include<stdlib.h>
//
//typedef char BTDataType;
//
//typedef struct BinaryTreeNode
//{
//	BTDataType _data;
//	struct BinaryTreeNode* _left;
//	struct BinaryTreeNode* _right;
//}BTNode;
//
////前序遍历
//void PrevOrder(BTNode* root)
//{
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	printf("%c ", root->_data);
//	PrevOrder(root->_left);
//	PrevOrder(root->_right);
//}
//
////中序遍历
//void InOrder(BTNode* root)
//{
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	InOrder(root->_left);
//	printf("%c ", root->_data);
//	InOrder(root->_right);
//}
//
////后序遍历
//void PostOrder(BTNode* root)
//{
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	PostOrder(root->_left);
//	PostOrder(root->_right);
//	printf("%c ", root->_data);
//}
//
//////计算树的结点的个数
////int TreeSize(BTNode* root)
////{
////	if (root == NULL)
////        return 0;
////
////	static int size = 0;
////	++size;
////	TreeSize(root->_left);
////	TreeSize(root->_right);
////
////	return size;
////}
//
//////计算树的结点的个数
////void TreeSize(BTNode* root, int* psize)
////{
////	if (root == NULL)
////		return 0;
////	else
////		(*psize)++;
////
////	TreeSize(root->_left, psize);
////	TreeSize(root->_right, psize);
////}
//
////计算树的结点的个数
//int TreeSize(BTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	else
//		return 1 + TreeSize(root->_left) + TreeSize(root->_right);
//}
//
////计算叶子结点的个数
//int TreeLeafSize(BTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	
//	if (root->_left == NULL && root->_right == NULL)
//		return 1;
//
//	return TreeLeafSize(root->_left) + TreeLeafSize(root->_right);
//}
//
//BTNode* CreatNode(int x)
//{
//	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
//	if (node == NULL)
//	{
//		printf("申请结点失败\n");
//		exit(-1);
//	}
//	node->_data = x;
//	node->_left = NULL;
//	node->_right = NULL;
//
//	return node;
//}
//
//int main()
//{
//	BTNode* A = CreatNode('A');
//	BTNode* B = CreatNode('B');
//	BTNode* C = CreatNode('C');
//	BTNode* D = CreatNode('D');
//	BTNode* E = CreatNode('E');
//	A->_left = B;
//	A->_right = C;
//	B->_left = D;
//	B->_right = E;
//
//	PrevOrder(A);
//	printf("\n");
//
//	InOrder(A);
//	printf("\n");
//
//	PostOrder(A);
//	printf("\n");
//
//	//printf("TreeSize:%d\n", TreeSize(A));//5
//	//printf("TreeSize:%d\n", TreeSize(A));//10
//
//	//int sizea = 0;
//	//TreeSize(A, &sizea);
//	//printf("TreeSize:%d\n", sizea);
//
//	//int sizeb = 0;
//	//TreeSize(B, &sizeb);
//	//printf("TreeSize:%d\n", sizeb);
//
//	printf("TreeSize:%d\n", TreeSize(A));
//	printf("TreeSize:%d\n", TreeSize(A));
//
//	printf("TreeLeafSize:%d\n", TreeLeafSize(A));
//
//	getchar();
//
//	return 0;
//}