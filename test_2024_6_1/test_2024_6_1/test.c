//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>


//删除有序链表中重复的元素
//给出一个升序排序的链表，删除链表中的所有重复出现的元素，只保留原链表中只出现一次的元素
//struct ListNode* deleteDuplicates(struct ListNode* head) 
//{
//    // write code here
//    if (head == NULL || head->next == NULL)
//    {
//        return head;
//    }
//
//    struct ListNode* prev = NULL;
//    struct ListNode* cur = head;
//    struct ListNode* next = cur->next;
//
//    while (next)
//    {
//        if (cur->val != next->val)
//        {
//            prev = cur;
//            cur = next;
//            next = next->next;
//        }
//        else
//        {
//            while (next && cur->val == next->val)
//            {
//                next = next->next;
//            }
//
//            if (prev)
//            {
//                prev->next = next;
//            }
//            else
//            {
//                head = next;
//            }
//
//            //释放
//            while (cur != next)
//            {
//                struct ListNode* del = cur;
//                cur = cur->next;
//                free(del);
//            }
//
//            if (next)
//            {
//                next = cur->next;
//            }
//        }
//    }
//
//    return head;
//}