//#define _CRT_SECURE_NO_WARNINGS 1
//
////数组形式的整数加法
//int* addToArrayForm(int* num, int numSize, int k, int* returnSize) {
//    int kSize = 0;
//    int kNum = k;
//    while (kNum)
//    {
//        ++kSize;
//        kNum /= 10;
//    }
//
//    int len = numSize > kSize ? numSize : kSize;
//    int* retArr = (int*)malloc(sizeof(int) * (len + 1));
//
//    int numi = numSize - 1;
//    int reti = 0;
//    int nextNum = 0;//进位
//    while (len--)
//    {
//        int a = 0;
//        if (numi >= 0)
//        {
//            a = num[numi];
//            numi--;
//        }
//
//        int ret = a + k % 10 + nextNum;
//        k /= 10;
//
//        if (ret > 9)
//        {
//            ret -= 10;
//            nextNum = 1;
//        }
//        else
//        {
//            nextNum = 0;
//        }
//
//        retArr[reti] = ret;
//        ++reti;
//    }
//
//    if (nextNum == 1)
//    {
//        retArr[reti] = 1;
//        ++reti;
//    }
//
//    int left = 0, right = reti - 1;
//    while (left < right)
//    {
//        int tmp = retArr[left];
//        retArr[left] = retArr[right];
//        retArr[right] = tmp;
//        ++left;
//        --right;
//    }
//
//    *returnSize = reti;
//    return retArr;
//}
//
////反转一个单链表
//struct ListNode* reverseList(struct ListNode* head) {
//    if (head == NULL || head->next == NULL)
//    {
//        return head;
//    }
//    struct ListNode* n1 = NULL, * n2 = head, * n3 = head->next;
//    while (n2)
//    {
//        //反转
//        n2->next = n1;
//
//        //迭代
//        n1 = n2;
//        n2 = n3;
//        if (n3)
//        {
//            n3 = n3->next;
//        }
//    }
//
//    return n1;
//}