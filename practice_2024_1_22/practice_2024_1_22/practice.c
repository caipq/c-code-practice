//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>


// 汉诺塔问题
// 
//void Move(char x, char y)
//{
//	printf("%c --> %c\n", x, y);
//}
//
//void HNtower(int a, char A, char B, char C)//汉诺塔函数实施主体,A为初始柱,B为经由柱,C为目的柱
//{
//	if (a == 1)
//	{
//		Move(A, C);
//	}
//	else
//	{
//		HNtower(a - 1, A, C, B);//第一个递归式
//		Move(A, C);
//		HNtower(a - 1, B, A, C);//第二个递归式
//	}
//}
//
//int MoveNum(int a)
//{
//	if (a == 1)
//		return 1;
//	else
//		return 2 * MoveNum(a - 1) + 1;//为了对应递归主题，用递归式表示
//}
//
//int main()
//{
//	int a = 0;
//	printf("请输入你选择的层数:>");
//	scanf("%d", &a);
//
//	int Num = MoveNum(a);
//
//	printf("%d层需要移动%d步\n", a, Num);
//	printf("具体过程如下\n");
//	HNtower(a, 'A', 'B', 'C');//进入递归函数
//	return 0;
//}

//void Move(char x, char y)
//{
//	printf("%c --> %c\n", x, y);
//}
//
//void HNtower(int a, char A, char B, char C)
//{
//	if (a == 1)
//		Move(A, C);
//	else
//	{
//		HNtower(a - 1, A, C, B);
//		Move(A, C);
//		HNtower(a - 1, B, A, C);
//	}
//}
//
//int MoveNum(int a)
//{
//	if (a == 1)
//		return 1;
//	else
//		return 2 * MoveNum(a - 1) + 1;
//}
//
//int main()
//{
//	int a = 0;
//	printf("请输入你选择的层数:>");
//	scanf("%d", &a);
//	int Num = MoveNum(a);
//
//	printf("%d层需要移动%d步\n", a, Num);
//	printf("具体过程如下\n");
//	HNtower(a, 'A', 'B', 'C');
//
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	for (a = 1, b = 1; a <= 100; a++)
//	{
//		if (b >= 20)
//			break;
//		if (b % 3 == 1)
//		{
//			b = b + 3;
//			continue;
//		}
//		b = b - 5;
//	}
//	printf("%d\n", a);
//	return 0;
//}


//编写程序数一下1到100的所有整数中出现多少个数字9

//int main()
//{
//
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)
//			count++;
//		if (i / 10 == 9)
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}


//计算1/1-1/2+1/3-1/4+1/5......+1/99-1/100的值，打印出结果

//int main()
//{
//	int i = 0;
//	double sum = 0.0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 2 == 0)
//			sum -= 1.0 / i;
//		else
//			sum += 1.0 / i;
//
//	}
//	printf("%lf\n", sum);
//
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	double sum = 0.0;
//	int flag = 1;
//	for (i = 1; i <= 100; i++)
//	{
//			sum += flag*1.0 / i;
//			flag = -flag;
//	}
//	printf("%lf\n", sum);
//
//	return 0;
//}


//求10个整数中最大值

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int max = arr[0];
//	int i = 0;
//	for (i = 1; i < 10; i++)
//	{
//		if (arr[i] > max)
//		{
//			max = arr[i];
//		}
//	}
//	printf("%d\n", max);
//
//	return 0;
//}


//在屏幕上输出9*9乘法口诀表
//
//int main()
//{
//	int i = 1;
//	for (i = 1; i <= 9; i++)
//	{
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//青蛙跳台阶问题


//递归
//int func(int n)
//{
//	if (n == 1)
//		return 1;
//	else if (n == 2)
//		return 2;
//	else
//		return func(n - 1) + func(n - 2);
//}
//
//int main()
//{
//
//	int x = 0;
//	printf("请选择青蛙需要跳的台阶数:>");
//	scanf("%d", &x);
//
//	int ret = func(x);
//	printf("func(%d)=%d\n", x, ret);
//	printf("总共有%d种跳法", ret);
//
//	return 0;
//}


//迭代（循环）
//int func(int n)
//{
//	int i = 1;
//	int j = 2;
//	int tmp = 0;
//	if (n == 1)
//		return i;
//	else if (n == 2)
//		return j;
//	else
//	{
//		while (n - 2)
//		{
//			tmp = i + j;
//			i = j;
//			j = tmp;
//			n--;
//		}
//		return tmp;
//	}
//}
//int main()
//{
//
//	int x = 0;
//	printf("请选择青蛙需要跳的台阶数:>");
//	scanf("%d", &x);
//
//	int ret = func(x);
//	printf("func(%d)=%d\n", x, ret);
//	printf("总共有%d种跳法", ret);
//
//	return 0;
//}

//void test(int arr[])
//{
//	arr[0] = 1;
//	arr[1] = 2;
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	test(arr);
//
//	return 0;
//}

//void test(int* p1, int* p2)
//{
//	*p1 = 1;
//	*p2 = 2;
//}
//
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	test(&a, &b);
//
//	return 0;
//}

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定，
//如：输入9，输出9*9口诀表，输出12，输出12*12的乘法口诀表

//void print_table(int n)
//{
//	int i = 0;
//	//行
//	for (i = 1; i <= n; i++)
//	{
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//函数
//	print_table(n);
//	//函数的起名是非常关键的，名字最好能体现函数的功能
//	return 0;
//}


//字符串逆序

//int my_strlen(char* str)
//{
//	int count = 0;
//	while(*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//循环
// 
//void reverse_string(char* str)
//{
//	int left = 0;
//	int right = my_strlen(str) - 1;
//
//	while (left < right)
//	{
//		char tmp = str[left];
//		str[left] = str[right];
//		str[right] = tmp;
//		left++;
//		right--;
//     }
//}

//void reverse_string(char* str)
//{
//	int left = 0;
//	int right = my_strlen(str) - 1;
//
//	while (left < right)
//	{
//		char tmp = *(str + left);//str[left]
//		*(str + left) = *(str + right);
//		*(str + right) = tmp;
//		left++;
//		right--;
//	}
//}


//递归

//void reverse_string(char* str)
//{
//	char tmp = *str;
//	int len = my_strlen(str);
//	*str = *(str + len - 1);
//	*(str + len - 1) = '\0';
//
//	//判断条件
//	if (my_strlen(str+1)>=2)
//	{
//		reverse_string(str + 1);
//	}
//	
//	*(str + len - 1) = tmp;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	reverse_string(arr);
//	printf("%s\n", arr);
//
//	return 0;
//}