//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//平衡二叉树
//给定一个二叉树，判断它是否是平衡二叉树
//本题中，一颗高度平衡二叉树定义为：
//一个二叉树每个结点的左右两个子树的高度差的绝对值不超过1
// O(N)
//int TreeDepth(struct TreeNode* root)
//{
//    if (root == NULL)
//        return 0;
//
//    int leftDepth = TreeDepth(root->left);
//    int rightDepth = TreeDepth(root->right);
//
//    return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
//}
// 最好是O(N)，最坏是O(N*N)
//bool isBalanced(struct TreeNode* root) 
//{
//    if (root == NULL)
//        return true;
//
//    int gap = TreeDepth(root->left) - TreeDepth(root->right);
//    if (abs(gap) > 1)
//        return false;
//
//    return isBalanced(root->left) && isBalanced(root->right);
//}

//优化到最坏是O(N)
//bool _isBalanced(struct TreeNode* root, int* pDepth)
//{
//    if (root == NULL)
//    {
//        *pDepth = 0;
//        return true;
//    }
//    else
//    {
//        int leftDepth = 0;
//        if (_isBalanced(root->left, &leftDepth) == false)
//        {
//            return false;
//        }
//
//        int rightDepth = 0;
//        if (_isBalanced(root->right, &rightDepth) == false)
//        {
//            return false;
//        }
//
//        if (abs(leftDepth - rightDepth) > 1)
//            return false;
//
//        *pDepth = leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
//        return true;
//    }
//}
//
//bool isBalanced(struct TreeNode* root) {
//    int depth = 0;
//    return _isBalanced(root, &depth);
//}