#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>


//union Un
//{
//	char a[5];
//	int i;
//};
//
//int main()
//{
//	union Un u;
//	printf("%d\n", sizeof(u));
//
//	return 0;
//}
//
//union Un
//{
//	short s[5];
//	int a;
//};
//
//int main()
//{
//	union Un u;
//	printf("%d\n", sizeof(u));
//
//	return 0;
//}
//
//#include <stdio.h>
//#include<string.h>
//
//替换空格
//请实现一个函数，将一个字符串中的每个空格替换成%20。例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy.
//
//void replaceSpace(char* str, int length)//str指向了要修改的字符串。length要修改的字符串长度
//{
//	char* cur = str;
//	int space_count = 0;
//	while (*cur)
//	{
//		if (*cur == ' ')
//			space_count++;
//		cur++;
//	}
//	int end1 = length - 1;//字符串的结束位置
//	int end2 = length + space_count * 2 - 1;//空格替换成%20会加两个单位，替换完成后字符串结束的位置
//	while (end1 != end2)//替换全部完成，两个位置的end指针的位置会相同
//	{
//		if (str[end1] != ' ')//end1指向空格end1不动，end2动3次
//		{
//			str[end2--] = str[end1--];//把end1所指向的内容给end2，两个指针往前移动，先赋值再减减
//		}
//		else
//		{
//			end1--;
//			str[end2--] = '0';//先赋值再减减
//			str[end2--] = '2';
//			str[end2--] = '%';
//		}
//	}
//}
//int main()
//{
//	char arr[50] = "we are happy.";
//	int length = strlen(arr);
//	replaceSpace(arr, length);
//	printf("%s\n", arr);
//	return 0;
//}
//
//#include <string.h>
//#include <stdio.h>
//void fun(char* p)
//{
//	while (*p != '\0')
//	{
//		int right = strlen(p) - 1; //计算最后面字符的下标
//		char* pr = p + right;
//		//先找到空格 ,没有空格就直接返回
//		while (*p != ' ')
//		{
//			p++;
//			if (*p == '\0')
//			{
//				return;
//			}
//		}
//		//把数据往后移动两位
//		while (pr != p)
//		{
//			*(pr + 2) = *(pr);
//			pr--;
//		}
//		*p = '%';
//		*(p + 1) = '2';
//		*(p + 2) = '0';
//		p += 3; //跳过 %20 三个字符
//	}
//
//}
//int main()
//{
//	char arr[100] = "abc def gh";
//	fun(arr);
//	printf("%s\n", arr);
//	return 0;
//}

