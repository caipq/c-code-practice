//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "Queue.h"
//
//typedef char BTDataType;
//
//typedef struct BinaryTreeNode
//{
//	BTDataType _data;
//	struct BinaryTreeNode* _left;
//	struct BinaryTreeNode* _right;
//}BTNode;
//
////前序遍历
//void PrevOrder(BTNode* root)
//{
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	printf("%c ", root->_data);
//	PrevOrder(root->_left);
//	PrevOrder(root->_right);
//}
//
////中序遍历
//void InOrder(BTNode* root)
//{
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	InOrder(root->_left);
//	printf("%c ", root->_data);
//	InOrder(root->_right);
//}
//
////后序遍历
//void PostOrder(BTNode* root)
//{
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	PostOrder(root->_left);
//	PostOrder(root->_right);
//	printf("%c ", root->_data);
//}
//
//// 层序遍历
//void BinaryTreeLevelOrder(BTNode* root)
//{
//	Queue q;
//	QueueInit(&q);
//	if (root == NULL)
//		return;
//
//	QueuePush(&q, root);
//
//	while (!QueueEmpty(&q))
//	{
//		BTNode* front = QueueFront(&q);
//		QueuePop(&q);
//
//		printf("%c ", front->_data);
//		if (front->_left)
//		{
//			QueuePush(&q, front->_left);
//		}
//
//		if (front->_right)
//		{
//			QueuePush(&q, front->_right);
//		}
//	}
//
//	QueueDestory(&q);
//	printf("\n");
//}
//
//// 判断二叉树是否是完全二叉树
////是，返回1；不是，返回0
//int BinaryTreeComplete(BTNode* root)
//{
//	Queue q;
//	QueueInit(&q);
//	if (root == NULL)
//		return 1;
//
//	QueuePush(&q, root);
//
//	while (!QueueEmpty(&q))
//	{
//		BTNode* front = QueueFront(&q);
//		QueuePop(&q);
//
//		if (front == NULL)
//		{
//			break;
//		}
//
//		QueuePush(&q, front->_left);
//		QueuePush(&q, front->_right);
//	}
//
//	while (!QueueEmpty(&q))
//	{
//		BTNode* front = QueueFront(&q);
//		QueuePop(&q);
//
//		if (front)
//		{
//			QueueDestory(&q);
//			return 0;
//		}
//	}
//
//	QueueDestory(&q);
//	return 1;
//}
//
//////计算树的结点的个数
////int TreeSize(BTNode* root)
////{
////	if (root == NULL)
////        return 0;
////
////	static int size = 0;
////	++size;
////	TreeSize(root->_left);
////	TreeSize(root->_right);
////
////	return size;
////}
//
//////计算树的结点的个数
////void TreeSize(BTNode* root, int* psize)
////{
////	if (root == NULL)
////		return 0;
////	else
////		(*psize)++;
////
////	TreeSize(root->_left, psize);
////	TreeSize(root->_right, psize);
////}
//
////计算树的结点的个数
//int TreeSize(BTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	else
//		return 1 + TreeSize(root->_left) + TreeSize(root->_right);
//}
//
////计算叶子结点的个数
//int TreeLeafSize(BTNode* root)
//{
//	if (root == NULL)
//		return 0;
//	
//	if (root->_left == NULL && root->_right == NULL)
//		return 1;
//
//	return TreeLeafSize(root->_left) + TreeLeafSize(root->_right);
//}
//
//// 二叉树第k层节点个数
//int BinaryTreeLevelKSize(BTNode* root, int k)
//{
//	if (root == NULL)
//		return 0;
//
//	if (k == 1)
//	{
//		return 1;
//	}
//
//	return BinaryTreeLevelKSize(root->_left, k - 1) + BinaryTreeLevelKSize(root->_right, k - 1);
//}
//
//// 二叉树查找值为x的节点
//BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
//{
//	if (root == NULL)
//	{
//		return NULL;
//	}
//
//	if (root->_data == x)
//	{
//		return root;
//	}
//
//	BTNode* node = BinaryTreeFind(root->_left, x);
//	if (node)
//	{
//		return node;
//	}
//
//	node = BinaryTreeFind(root->_right, x);
//	if (node)
//	{
//		return node;
//	}
//
//	return NULL;
//}
//
//void DestoryTree(BTNode* root)
//{
//	if (root == NULL)
//		return;
//
//	DestoryTree(root->_left);
//	DestoryTree(root->_right);
//	free(root);
//}
//
//BTNode* CreatNode(int x)
//{
//	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
//	if (node == NULL)
//	{
//		printf("申请结点失败\n");
//		exit(-1);
//	}
//	node->_data = x;
//	node->_left = NULL;
//	node->_right = NULL;
//
//	return node;
//}
//
//int main()
//{
//	BTNode* A = CreatNode('A');
//	BTNode* B = CreatNode('B');
//	BTNode* C = CreatNode('C');
//	BTNode* D = CreatNode('D');
//	BTNode* E = CreatNode('E');
//	A->_left = B;
//	A->_right = C;
//	B->_left = D;
//	B->_right = E;
//
//	PrevOrder(A);
//	printf("\n");
//
//	InOrder(A);
//	printf("\n");
//
//	PostOrder(A);
//	printf("\n");
//
//	//printf("TreeSize:%d\n", TreeSize(A));//5
//	//printf("TreeSize:%d\n", TreeSize(A));//10
//
//	//int sizea = 0;
//	//TreeSize(A, &sizea);
//	//printf("TreeSize:%d\n", sizea);
//
//	//int sizeb = 0;
//	//TreeSize(B, &sizeb);
//	//printf("TreeSize:%d\n", sizeb);
//
//	printf("TreeSize:%d\n", TreeSize(A));
//	printf("TreeSize:%d\n", TreeSize(A));
//
//	printf("TreeLeafSize:%d\n", TreeLeafSize(A));
//	printf("BinaryTreeLevelKSizee:%d\n", BinaryTreeLevelKSize(A, 3));
//
//	BinaryTreeLevelOrder(A);
//
//	printf("BinaryTreeComplete:%d\n", BinaryTreeComplete(A));
//	
//
//	getchar();
//
//	return 0;
//}