//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//汉诺塔问题
//
//int MoveNum(int n)
//{
//	if (n == 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return 2 * MoveNum(n - 1) + 1;
//	}
//}
//
//void Move(char x, char y)
//{
//	printf("%c ---> %c\n", x, y);
//}
//
//void HNtower(int n, char A, char B, char C)
//{
//	if (n == 1)
//	{
//		Move(A, C);
//	}
//	else
//	{
//		HNtower(n - 1, A, C, B);
//		Move(A, C);
//		HNtower(n - 1, B, A, C);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	printf("请输入你要选择的层数:>");
//	scanf("%d", &n);
//	
//	int Num = MoveNum(n);
//	printf("%d层需要移动%d步\n", n, Num);
//
//	printf("具体过程如下\n");
//	HNtower(n, 'A', 'B', 'C');
//
//	return 0;
//}

//编写程序数一下1到100的所有整数中出现多少个数字9

//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)
//		{
//			count++;
//		}
//		if (i / 10 == 9)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}

//计算1/1-1/2+1/3-1/4+1/5......+1/99-1/100的值，打印出结果

//int main()
//{
//	int i = 0;
//	double sum = 0.0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 2 == 0)
//		{
//			sum += 1.0 / i;
//		}
//		else
//		{
//			sum -= 1.0 / i;
//		}
//	}
//	printf("%lf\n", sum);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	double sum = 0.0;
//	int flag = 1;
//	for (i = 1; i <= 100; i++)
//	{
//		sum += flag * 1.0 / i;
//		flag = -flag;
//	}
//	printf("%lf\n", sum);
//	return 0;
//}

//求10个整数中的最大值

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int max = arr[0];
//	int i = 0;
//	for (i = 1; i < 10; i++)
//	{
//		if (arr[i] > max)
//		{
//			max = arr[i];
//		}
//	}
//	printf("%d\n", max);
//	return 0;
//}

//在屏幕上输出9*9乘法口诀表

//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 9; i++)
//	{
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//青蛙跳台阶问题

//递归

//int func(int x)
//{
//	if (x == 1)
//	{
//		return 1;
//	}
//	else if (x == 2)
//	{
//		return 2;
//	}
//	else
//	{
//		return func(x - 1) + func(x - 2);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	printf("请选择青蛙需要跳的台阶数:>");
//	scanf("%d", &n);
//
//	int ret = func(n);
//	printf("func(%d)=%d\n", n, ret);
//	printf("总共有%d种跳法\n", ret);
//
//	return 0;
//}

//非递归（迭代/循环）

//int func(int x)
//{
//	int i = 1;
//	int j = 2;
//	int tmp = 0;
//	if (x == 1)
//	{
//		return i;
//	}
//	else if (x == 2)
//	{
//		return j;
//	}
//	else
//	{
//		while (x > 2)
//		{
//			tmp = i + j;
//			i = j;
//			j = tmp;
//			x--;
//		}
//		return tmp;
//	}
//}
//
//int main()
//{
//	int n = 0;
//	printf("请选择青蛙需要跳的台阶数:>");
//	scanf("%d", &n);
//
//	int ret = func(n);
//	printf("func(%d)=%d\n", n, ret);
//	printf("总共有%d种跳法\n", ret);
//
//	return 0;
//}

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定

//void print_table(int n)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= n; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int n = 0;
//	printf("请输入你要生成的乘法口诀表的大小:>");
//	scanf("%d", &n);
//
//	print_table(n);
//
//	return 0;
//}

//字符串逆序

//非递归（循环）
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//void reverse_string(char* str)
//{
//	int left = 0;
//	int right = my_strlen(str) - 1;
//	while (left < right)
//	{
//		int tmp = str[left];
//		str[left] = str[right];
//		str[right] = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//
//	reverse_string(arr);
//	printf("%s\n", arr);
//
//	return 0;
//}

//递归
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//void reverse_string(char* str)
//{
//	char tmp = *str;
//	int len = my_strlen(str);
//	*str = *(str + len - 1);
//	*(str + len - 1) = '\0';
//	if (my_strlen(str + 1) >= 2)
//	{
//		reverse_string(str + 1);
//	}
//	*(str + len - 1) = tmp;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//
//	reverse_string(arr);
//	printf("%s\n", arr);
//
//	return 0;
//}