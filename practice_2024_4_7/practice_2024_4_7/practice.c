#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//
//int main()
//{
	//printf("%s\n", __FILE__);
	//printf("%d\n", __LINE__);
	//printf("%s\n", __DATE__);
	//printf("%s\n", __TIME__);
	//printf("%s\n", __FUNCTION__);

	//int i = 0;
	//FILE* pf = fopen("log.txt", "a+");
	//if (pf == NULL)
	//{
	//	perror("fopen\n");
	//	return 1;
	//}
	//for (i = 0; i < 10; i++)
	//{
	//	fprintf(pf, "%s %d %s %s %d\n", __FILE__, __LINE__, __DATE__, __TIME__, i);
	//}
	//fclose(pf);
	//pf = NULL;

	//printf("%d\n", __STDC__);//不支持

//	return 0;
//}

//#define 是定义符号的
//
//#define M 1000
//#define reg register
//#define do_forever for(;;)
////
//int main()
//{
//	reg int num = 0;
//	do_forever;
//	int m = M;
//	printf("%d\n", m);
//
//	return 0;
//}
//
//#define CASE break;case
//
//int main()
//{
//	int n = 0;
//	switch (n)
//	{
//	case 1:
//	CASE 2:
//	CASE 3:
//	}
//
//	return 0;
//}

//#define 定义宏
//括号很重要

//#define SQUARE(X) X*X
//
//int main()
//{
//	printf("%d\n", SQUARE(3));//9
//	//printf("%d\n", 3 * 3);
//
//	printf("%d\n", SQUARE(3 + 1));//7
//	//printf("%d\n", 3 + 1 * 3 + 1);
//
//	return 0;
//}

//#define SQUARE(X) ((X)*(X))
//
//int main()
//{
//	printf("%d\n", SQUARE(3));//9
//
//	printf("%d\n", SQUARE(3 + 1));//16
//
//	return 0;
//}

//#define DOUBLE(X) (X)+(X)
//
//int main()
//{
//	printf("%d\n", 10 * DOUBLE(4));//44
//	//printf("%d\n", 10 * (4) + (4));
//
//	return 0;
//}
//
//#define DOUBLE(X) ((X)+(X))
//
//int main()
//{
//	printf("%d\n", 10 * DOUBLE(4));//80
//
//	return 0;
//}
//
//#define M 100
//#define MAX(X, Y) ((X)>(Y)?(X):(Y))
//
//int main()
//{
//	int max = MAX(101, M);
//	printf("M = %d\n", M);//第一个M不会被替换成100
//
//	return 0;
//}

//void print(int x)
//{
//	printf("the value of x is %d\n", x);//做不到
//}
//
//int main()
//{
//	printf("hello world\n");
//	printf("hello" "world\n");
//
//	int a = 10;
//	print(a);
//	//the value of a is 10
//	int b = 20;
//	print(b);
//	//the value of b is 20
//	int c = 30;
//	print(c);
//	//the value of c is 30
//	return 0;
//}
//
//#define PRINT(X, FORMAT) printf("the value of "#X" is "FORMAT"\n", X)
//
//int main()
//{
//	printf("hello world\n");
//	printf("hello " "world\n");
//
//	int a = 10;
//	PRINT(a, "%d");
//	//printf("the value of ""a"" is %d\n", a);
//	//the value of a is 10
//
//	int b = 20;
//	PRINT(b, "%d");
//	//the value of b is 20
//
//	int c = 30;
//	PRINT(c, "%d");
//	//the value of c is 30
//
//	float f = 5.5f;
//	PRINT(f, "%f");
//	//printf("the value of ""f"" is ""%f""\n", f);
//
//	return 0;
//}

//#define CAT(X,Y) X##Y
//
//int main()
//{
//	int class101 = 100;
//	printf("%d\n", CAT(class, 101));
//
//	return 0;
//}
//
//#define MAX(X,Y) ((X)>(Y)?(X):(Y))//效率更高
//
//int Max(int x, int y)
//{
//	return x > y ? x : y;
//}
//	
//int main()
//{
//	int a = 5;
//	int b = 8;
//	int m = MAX(a++, b++);
//	printf("a = %d b = %d\n", a, b);//6 10
//	printf("m = %d\n", m);//9
//
//	return 0;
//}
//
//#define MALLOC(num, type) (type*)malloc(num * sizeof(type))
//
//int main()
//{
//	//malloc(10 * sizeof(int));
//	int* p = MALLOC(10, int);
//	
//	return 0;
//}
//
//#define M 100
//
//int main()
//{
//	int a = M;
//#undef M
//	printf("%d\n", M);
//
//	return 0;
//}

//条件编译
//#define PRINT
//
//int main()
//{
//#ifdef PRINT
//	printf("hehe\n");
//#endif
//
//#if 1
//	printf("hehe\n");
//#endif
//
//#if 1-2
//	printf("hehe\n");
//#endif
// 
//#if 0
//	printf("hehe\n");
//#endif
//
//	return 0;
//}
//
//#define PRINT 1
//
//int main()
//{
//#if PRINT
//	printf("hehe\n");
//#endif
//
//	return 0;
//}
//
//int main()
//{
//#if 1==1
//	printf("hehe\n");
//#elif 1==2
//	printf("haha\n");
//#else
//	printf("heihei\n");
//#endif
//
//	return 0;
//}
//
//#define TEST 0
//#define HEHE 1
//int main()
//{
////如果TEST定义了，下面参与编译(值是多少不重要)
////1
//#ifdef TEST
//	printf("test1\n");
//#endif
////2
//#if defined(TEST)
//	printf("test2\n");
//#endif
//
////如果HEHE不定义，下面参与编译
////3
//#ifndef HEHE
//		printf("hehe1\n");
//#endif
////4
//#if !defined(HEHE)
//		printf("hehe2\n");
//#endif
//
//	return 0;
//}