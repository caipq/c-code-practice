#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>

//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//int main()
//{
//	struct S s = { 0 };
//	printf("%d\n", sizeof(s));
//
//	return 0;
//}
//
//int main()
//{
//	int a1 = 3 / 2;
//	float a2 = 3.0 / 2.0;
//	printf("%d\n", a1);
//	printf("%lf\n", a2);
//	return 0;
//}
//
//int main()
//{
//    int a = -10;
//	//把a的二进制向右移动1位
//	int b = a >> 1;
//	//当前的右移操作符使用的是算术右移
//	printf("b = %d\n", b);
//	return 0;
//}

//默认对齐数是8

//把默认对齐数改为2
//
//#pragma pack(2)
//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()
//
//int main()
//{
//	printf("%d\n", sizeof(struct S));
//
//	return 0;
//}

//#include<stddef.h>
//
//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//int main()
//{
//	printf("%d\n", offsetof(struct S, c1));
//	printf("%d\n", offsetof(struct S, i));
//	printf("%d\n", offsetof(struct S, c2));
//
//	return 0;
//}

//写一个宏，计算结构体中某变量相对于首地址的偏移，并给出说明

//#include <stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a = %d b = %d\n", a, b);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int num = -1;
//	int i = 0;
//	int count = 0;//计数
//	for (i = 0; i < 32; i++)
//	{
//		if (num & (1 << i))
//			count++;
//	}
//	printf("二进制中1的个数 = %d\n", count);
//	return 0;
//}
//
//#include <stdio.h>
//int main()
//{
//	int num = -1;
//	int i = 0;
//	int count = 0;//计数
//	while (num)
//	{
//		count++;
//		num = num & (num - 1);
//	}
//	printf("二进制中1的个数 = %d\n", count);
//	return 0;
//}