//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>

//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (i = 5)
//			printf("%d", i);
//	}
//	return 0;
//}


//int func(int a)
//{
//	int b;
//	switch (a)
//	{
//	case 1:
//		b = 30;
//	case 2:
//		b = 20;
//	case 3:
//		b = 16;
//	default:
//		b = 0;
//	}
//	return b;
//}
//
//int main()
//{
//	printf("%d\n", func(1));
//
//	return 0;
//}

//F11 - 逐语句
//F10 - 逐过程

//int main()
//{
//	int x = 3;
//	int y = 3;
//	switch (x % 2)
//	{
//	case 1:
//		switch (y)
//		{
//		case 0:
//			printf("first");
//		case 1:
//			printf("second");
//			break;
//		default:
//			printf("hello");
//		}
//	case 2:
//		printf("third");
//	}
//
//	return 0;
//}

//题目名称：
//从大到小输出
//题目内容：
//写代码将三个整数按从大到小输出

//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a < b)
//	{
//		int tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		int tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//	}
//	
//	printf("%d %d %d\n", a, b, c);
//
//	return 0;
//}

//写一个代码打印1-100之间所有3的倍数的数字

//int main()
//{
//	int i = 0;
//	//for (i = 1; i < 100; i++)
//	//{
//	//	if (i % 3 == 0)
//	//		printf("%d ", i);
//	//}
//
//	for (i = 3; i < 100; i+=3)
//	{
//			printf("%d ", i);
//	}
//	return 0;
//}

//给定两个数，求这两个数的最大公约数

//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d%d", &m, &n);
//	int max = m < n ? m : n;
//	//假设最大公约数max就是m和n的较小值
//	//int max = 0;
//	//if (m > n)
//	//	max = n;
//	//else
//	//	max = m;
//
//	while (1)
//	{
//		if (m % max == 0 && n % max == 0)
//		{
//			printf("最大公约数是： %d\n", max);
//			break;
//		}
//		max--;
//	}
//
//	return 0;
//}

//辗转相除法

//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d%d", &m, &n);
//	int t = 0;
//	while (t = m % n)
//	{
//		m = n;
//		n = t;
//	}
//	printf("最大公约数：%d\n", n);
//	//最小公倍数 = m*n/最大公约数
//
//	return 0;
//}

//#include<stdlib.h>
//#include<time.h>
//void menu()
//{
//	printf("*****************************\n");
//	printf("*******  1.play       *******\n");
//	printf("*******  0.exit       *******\n");
//	printf("*****************************\n");
//}
//
//void game()
//{
//	int ret = rand() % 100 + 1;
//	int guess = 0;
//	while (1)
//	{
//		printf("请选择数字:>");
//		scanf("%d", &guess);
//		if (guess > ret)
//		{
//			printf("猜大了\n");
//		}
//		else if (guess < ret)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf(" 恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//	} while (input);
//		return 0;
//}