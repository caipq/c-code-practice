//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "SeqList.h"
//
//void TestSeqList1()
//{
//	SeqList s = { 0 };
//	SeqListInit(&s);
//	SeqListPushBack(&s, 1);
//	SeqListPushBack(&s, 2);
//	SeqListPushBack(&s, 3);
//	SeqListPushBack(&s, 4);
//	SeqListPushBack(&s, 5);
//	SeqListPrint(&s);
//
//	SeqListPopBack(&s);
//	SeqListPopBack(&s);
//	SeqListPrint(&s);
//
//	SeqListPushFront(&s, -1);
//	SeqListPushFront(&s, -2);
//	SeqListPushFront(&s, -3);
//	SeqListPrint(&s);
//
//	SeqListPopFront(&s);
//	SeqListPopFront(&s);
//	SeqListPrint(&s);
//
//	int pos = SeqListFind(&s, 1);
//	if (pos != -1)
//	{
//		SeqListErase(&s, pos);
//	}
//	SeqListPrint(&s);
//}
//
//int main()
//{
//	TestSeqList1();
//	return 0;
//}