//#pragma once
//
//#include<stdio.h>
//#include<assert.h>
//#include<stdlib.h>
//
//typedef int SLDataType;
//
//typedef struct SeqList
//{
//	SLDataType* a;
//	int size;
//	int capacity;
//}SL, SeqList;
//
////初始化
//void SeqListInit(SL* ps);
//
////打印
//void SeqListPrint(SL* ps);
//
////销毁
//void SeqListDestory(SL* ps);
//
////增容
//void SeqListCheckCapacity(SL* ps);
//
////尾插尾删
//void SeqListPushBack(SL* ps, SLDataType x);
//void SeqListPopBack(SL* ps);
//
////头插头删
//void SeqListPushFront(SL* ps, SLDataType x);
//void SeqListPopFront(SL* ps);
//
////在任意位置的插入删除
//void SeqListInert(SL* ps, int pos, SLDataType x);
//void SeqListErase(SL* ps, int pos);
//
////顺序查找
//int SeqListFind(SL* ps, SLDataType x);