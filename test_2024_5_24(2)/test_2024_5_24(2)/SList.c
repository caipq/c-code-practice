//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "SList.h"
//
////打印
//void SListPrint(SListNode* phead)
//{
//	SListNode* cur = phead;
//	while (cur != NULL)
//	{
//		printf("%d->", cur->data);
//		cur = cur->next;
//	}
//	printf("NULL\n");
//}
//
//SListNode* BuySListNode(SListDataType x)
//{
//	SListNode* newNode = (SListNode*)malloc(sizeof(SListNode));
//	if (newNode == NULL)
//	{
//		printf("申请结点失败\n");
//		exit(-1);
//	}
//
//	newNode->data = x;
//	newNode->next = NULL;
//
//	return newNode;
//}
//
////尾插尾删
//void SListPushBack(SListNode** pphead, SListDataType x)
//{
//	SListNode* newNode = BuySListNode(x);
//
//	if (*pphead == NULL)
//	{
//		*pphead = newNode;
//	}
//	else
//	{
//		SListNode* tail = *pphead;
//		while (tail->next != NULL)
//		{
//			tail = tail->next;
//		}
//
//		tail->next = newNode;
//	}
//}
//
//void SListPopBack(SListNode** pphead)
//{
//	if (*pphead == NULL)
//	{
//		return;
//	}
//	else if ((*pphead)->next == NULL)
//	{
//		free(*pphead);
//		*pphead = NULL;
//	}
//	else
//	{
//		SListNode* prev = NULL;
//		SListNode* tail = *pphead;
//		while (tail->next != NULL)
//		{
//			prev = tail;
//			tail = tail->next;
//		}
//
//		free(tail);
//		prev->next = NULL;
//	}
//}
//
////头插头删
//void SListPushFront(SListNode** pphead, SListDataType x)
//{
//	SListNode* newNode = BuySListNode(x);
//	newNode->next = *pphead;
//	*pphead = newNode;
//}
//void SListPopFront(SListNode** pphead)
//{
//	if (*pphead == NULL)
//	{
//		return;
//	}
//	else
//	{
//		SListNode* next = (*pphead)->next;
//		free(*pphead);
//		*pphead = next;
//	}
//}
//
////查找
//SListNode* SListFind(SListNode* phead, SListDataType x)
//{
//	assert(phead);
//
//	SListNode* cur = phead;
//	while (cur != NULL)
//	{
//		if (cur->data == x)
//		{
//			return cur;
//		}
//
//		cur = cur->next;
//	}
//
//	return NULL;
//}
//
//// 单链表在pos位置之后插入x
//// 分析思考为什么不在pos位置之前插入？
//void SListInsertAfter(SListNode* pos, SListDataType x)
//{
//	assert(pos);
//	SListNode* newNode = BuySListNode(x);
//	newNode->next = pos->next;
//	pos->next = newNode;
//}
//
//
//// 单链表删除pos位置之后的值
//// 分析思考为什么不删除pos位置？
//void SListEraseAfter(SListNode* pos)
//{
//	assert(pos);
//
//	if (pos->next)
//	{
//		SListNode* next = pos->next;
//		SListNode* nextnext = next->next;
//		pos->next = nextnext;
//		free(next);
//	}
//}