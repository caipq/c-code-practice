//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "Heap.h"
//
//void Swap(HPDataType* p1, HPDataType* p2)
//{
//	HPDataType tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
////前提：左右子树都是小堆
//void AdjustDown(HPDataType* a, int n, int root)
//{
//	int parent = root;
//	int child = parent * 2 + 1;//左孩子
//	while (child < n)
//	{
//		//找出左右孩子中小的那一个
//		if (child + 1 < n && a[child + 1] < a[child])
//		{
//			++child;
//		}
//
//		//如果孩子小于父亲则交换
//		if (a[child] < a[parent])
//		{
//			Swap(&a[child], &a[parent]);
//			parent = child;
//			child = parent * 2 + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}
//
//void HeapInit(Heap* php, HPDataType* a, int n)
//{
//	php->_a = (HPDataType*)malloc(sizeof(HPDataType) * n);
//	//...
//	memcpy(php->_a, a, sizeof(HPDataType) * n);
//	php->_size = n;
//	php->_capacity = n;
//
//	//构建堆
//	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
//	{
//		AdjustDown(php->_a, php->_size, i);
//	}
//}
//
//void HeapDestory(Heap* php);
//
//void HeapPush(Heap* php, HPDataType x);
//
//void HeapPop(Heap* php);
//
//HPDataType HeapTop(Heap* php);