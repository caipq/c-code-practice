//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//int removeElement(int* nums, int numsSize, int val) 
//{
//    int src = 0, dst = 0;
//    while (src < numsSize)
//    {
//        if (nums[src] != val)
//        {
//            nums[dst] = nums[src];
//            src++;
//            dst++;
//        }
//        else
//        {
//            src++;
//        }
//    }
//
//    return dst;
//}

//相交链表
//给你两个单链表的头节点headA和headB，请你找出并返回两个单链表相交的起始节点。如果两个链表不存在相交节点，返回null。
//typedef struct ListNode ListNode;
//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
//    ListNode* curA = headA;
//    int la = 0;
//    while (curA)
//    {
//        ++la;
//        curA = curA->next;
//    }
//
//    ListNode* curB = headB;
//    int lb = 0;
//    while (curB)
//    {
//        ++lb;
//        curB = curB->next;
//    }
//
//    ListNode* longList = headA;
//    ListNode* shortList = headB;
//    if (lb > la)
//    {
//        longList = headB;
//        shortList = headA;
//    }
//
//    int gap = abs(la - lb);
//    while (gap--)
//    {
//        longList = longList->next;
//    }
//
//    while (longList)
//    {
//        if (longList == shortList)
//        {
//            return longList;
//        }
//
//        longList = longList->next;
//        shortList = shortList->next;
//    }
//
//    return NULL;
//}

//环状链表
//给定一个链表，判断链表中是否有环。
//bool hasCycle(struct ListNode* head) {
//    struct ListNode* slow = head;
//    struct ListNode* fast = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//
//        if (slow == fast)
//        {
//            return true;
//        }
//    }
//
//    return false;
//}

//环状链表2
//给定一个链表，返回链表开始入环的第一个结点。如果链表无环，则返回NULL
//struct ListNode* detectCycle(struct ListNode* head) {
//    struct ListNode* slow, * fast;
//    slow = fast = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//
//        if (fast == slow)
//        {
//            break;
//        }
//    }
//
//    if (fast == NULL || fast->next == NULL)
//    {
//        return NULL;
//    }
//
//    struct ListNode* meet = fast;
//
//    while (head != meet)
//    {
//        head = head->next;
//        meet = meet->next;
//    }
//
//    return meet;
//}