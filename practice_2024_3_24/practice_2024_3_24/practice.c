//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>
//#include<errno.h>
//#include<string.h>
//
//int main()
//{
//	//打开文件失败的时候，会返回NULL
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		//printf("%s\n", strerror(errno));
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//#include<ctype.h>
//
//int main()
//{
//	char ch = '#';
//	int ret = isdigit(ch);
//	printf("%d\n", ret);
//	return 0;
//}
//
//int main()
//{
//	char arr[20] = { 0 };
//	scanf("%s", arr);
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		if (isupper(arr[i]))
//		{
//			arr[i] = tolower(arr[i]);
//		}
//		printf("%c ", arr[i]);
//		i++;
//	}
//	return 0;
//}

//内存操作函数
//memcpy - 内存拷贝
//
//#include<assert.h>
//
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//		
//		//*((char*))dest++ = *((char*))src++;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[10] = { 0 };
//
//	my_memcpy(arr2, arr1, 20);
//
//	return 0;
//}
//
//int main()
//{
//	printf("%d\n", strlen("abcdef"));
//	// \62被解析成一个转义字符
//	printf("%d\n", strlen("c:\test\628\test.c"));
//	return 0;
//}
//
//int main()
//{
//	int i = 10;
//	do
//	{
//		printf("%d\n", i);
//	} while (i < 10);
//	return 0;
//}

//int main()
//{
//	printf("%d", printf("%d", printf("%d", 43)));
//
//	return 0;
//}
//
//#include<assert.h>
//
////memcpy函数应该拷贝不重叠的内存
////memmove函数可以拷贝内存重叠的情况
//
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	if (dest < src)
//	{
//		//前->后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后->前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//
////memcpy - 只要实现了不重叠拷贝就可以了，而VS中的实现既可以拷贝不重叠，也可以拷贝重叠内存
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	//my_memmove(arr1 + 2, arr1, 20);
//	//my_memmove(arr1, arr1 + 2, 20);
//	memcpy(arr1 + 2, arr1, 20);
//
//	return 0;
//}

//memcmp - 内存比较
//
//int main()
//{
//	float arr1[] = { 1.0,2.0,3.0,4.0 };
//	float arr2[] = { 1.0,3.0 };
//	int ret = memcmp(arr1, arr2, 4);
//	//memcmp - strcmp
//	printf("%d\n", ret);
//
//	return 0;
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	memset(arr, 1, 20);//以字节为单位设置内存的
//
//	return 0;
//}