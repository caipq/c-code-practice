#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>

//模拟实现strcpy
// 
//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;
//}

//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest++ = *src++;//hello的拷贝
//	}
//	*dest = *src;// \0的拷贝
//}

//void my_strcpy(char* dest, char* src)
//{
//	while (*dest++ = *src++)
//	{
//		;//hello的拷贝
//	}
//}
//
//
//int main()
//{
//	char arr1[20] = "xxxxxxxxxxx";
//	char arr2[] = "hello";
//	my_strcpy(arr1, arr2);//1.目标空间的起始地址 2.源空间的起始地址
//	printf("%s\n", arr1);
//	return 0;
//}

//#include<assert.h>

//const 修饰变量，这个变量就被称为常变量，但本质上还是变量
//const修饰指针变量的时候
//const如果放在*的左边，修饰的是*p，表示指针指向的内容，是不能通过指针来改变的
//     但是指针变量本身p是可以修改的
//void my_strcpy(char* dest, const char* src)
//{
//	assert(dest != NULL);//断言
//	assert(src != NULL);//断言
//	while (*dest++ = *src++)
//	{
//		;//hello的拷贝
//	}
//}
//
//
//int main()
//{
//	char arr1[20] = "xxxxxxxxxxx";
//	char arr2[] = "hello";
//	my_strcpy(arr1, NULL);//1.目标空间的起始地址 2.源空间的起始地址
//	printf("%s\n", arr1);
//	return 0;
//}