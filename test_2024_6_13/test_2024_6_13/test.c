//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//设计一个算法，找出数组中最小的k个数。以任意顺序返回这k个数均可。

//void AdjustDown(int* a, int n, int root)
//{
//    int parent = root;
//    int child = parent * 2 + 1;
//
//    while (child < n)
//    {
//        if (child + 1 < n && a[child + 1] > a[child])
//        {
//            child++;
//        }
//
//        if (a[child] > a[parent])
//        {
//            int tmp = a[child];
//            a[child] = a[parent];
//            a[parent] = tmp;
//
//            parent = child;
//            child = parent * 2 + 1;
//        }
//        else
//        {
//            break;
//        }
//    }
//}
//
//int* smallestK(int* arr, int arrSize, int k, int* returnSize) {
//    *returnSize = k;
//
//    int* retArr = (int*)malloc(sizeof(int) * k);
//
//    if (k == 0)
//        return NULL;
//
//    //建k个数的大堆
//    for (int i = 0; i < k; i++)
//    {
//        retArr[i] = arr[i];
//    }
//
//    for (int i = (k - 1 - 1) / 2; i >= 0; i--)
//    {
//        AdjustDown(retArr, k, i);
//    }
//
//    for (int j = k; j < arrSize; j++)
//    {
//        if (arr[j] < retArr[0])
//        {
//            retArr[0] = arr[j];
//            AdjustDown(retArr, k, 0);
//        }
//    }
//
//    return retArr;
//}
