//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "Queue.h"
//
////栈的作用
////1.先进先出的场景，比如要保持序列公平，排队抽好机
////2.广度优先遍历
//
//void TestQueue()
//{
//	Queue q;
//	QueueInit(&q);
//
//	QueuePush(&q, 1);
//	QueuePush(&q, 2);
//	QueuePush(&q, 3);
//	QueuePush(&q, 4);
//
//	while (!QueueEmpty(&q))
//	{
//		printf("%d ", QueueFront(&q));
//		QueuePop(&q);
//	}
//	printf("\n");
//
//	QueueDestory(&q);
//}
//
//int main()
//{
//	TestQueue();
//	return;
//}