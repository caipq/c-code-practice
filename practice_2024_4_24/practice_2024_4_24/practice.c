//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>
//
//struct S
//{
//	char a;
//	char b;
//	int c;
//	double d;
//};
//
//#define OFFSETOF(struct_name, mem_name) (int)&(((struct_name*)0)->mem_name)
//
//int main()
//{
//	struct S s = { 0 };
//	printf("%d\n", OFFSETOF(struct S, a));
//	printf("%d\n", OFFSETOF(struct S, b));
//	printf("%d\n", OFFSETOF(struct S, c));
//	printf("%d\n", OFFSETOF(struct S, d));
//	return 0;
//}
//
//struct A
//{
//	int _a : 2;//_a 占2个bit位
//	int _b : 5;//_b 占5个bit位
//	int _c : 10;//_c 占10个bit位
//	int _d : 30;//_d 占30个bit位
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//	return 0;
//}
//
//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//
//int main()
//{
//	struct S s = { 0 };
//	s.a = 10;
//	s.b = 12;
//	s.c = 3;
//	s.d = 4;
//	return 0;
//}
//
//enum Day//星期
//{
//	Mon,
//	Tues,
//	Wed,
//	Thur,
//	Fri,
//	Sat,
//	Sun
//};
//
//enum Color//颜色
//{
//	RED = 1,
//	GREEN = 2,
//	BLUE = 4
//};
//
//enum Color
//{
//	RED,
//	GREEN,
//	BLUE
//};
//
//int main()
//{
//	enum Color c = BLUE;
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);
//
//	return 0;
//}
//
//union Un
//{
//	char c;
//	int i;
//};
//
//union Un un;
//
//int main()
//{
//	printf("%p\n", &(un.i));
//	printf("%p\n", &(un.c));
//	return 0;
//}
//
//union Un1
//{
//	char c[5];
//	int i;
//};
//
//union Un2
//{
//	short c[7];
//	int i;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(union Un1));
//	printf("%d\n", sizeof(union Un2));
//	return 0;
//}