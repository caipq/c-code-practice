//#define _CRT_SECURE_NO_WARNINGS 1

//#include<iostream>
////using namespace std;//C++库中所有东西都是放到std命名空间中
//
//int main()
//{
//	std::cout << "hello world\n" << std::endl;
//	int i = 1;
//  double d = 1.11;
//  std::cout << i << " " << d << std::endl;
// 
//	return 0;
//}

//#include<iostream>
//using namespace std;//C++库中所有东西都是放到std命名空间中
//
//int main()
//{
//	cout << "hello world\n" << endl;
// 	int i = 1;
//  double d = 1.11;
//  cout << i << " " << d << endl;
//
//	return 0;
//}

//#include<iostream>
////using namespace std;//C++库中所有东西都是放到std命名空间中
//
//int cout = 10;
//int main()
//{
//	std::cout << "hello world\n" << std::endl;
//	int i = 1;
//	double d = 1.11;
//	std::cout << i << " " << d << std::endl;
//
//	return 0;
//}

//#include<iostream>
////using namespace std;//C++库中所有东西都是放到std命名空间中
//
//using std::cout;
//using std::endl;
//
//int cout = 10;
//int main()
//{
//	std::cout << "hello world\n" << std::endl;
//	int i = 1;
//	double d = 1.11;
//
//	std::cin >> i >> d;
//	std::cout << i << " " << d << std::endl;
//
//	return 0;
//}

//#include<iostream>
//using namespace std;

//缺省参数 -> 备胎
//void Func(int a = 0)
//{
//	cout << a << endl;
//}

////全缺省
//void Func1(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//}
//
////半缺省 必须从右往左缺省
//void Func2(int a, int b = 10, int c = 20)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//}
//
//int main()
//{
//	//cout << "hello world" << endl;
//
//	//Func1();
//	//Func1(1);
//	//Func1(1, 2);
//	//Func1(1, 2, 3);
//
//	Func2(1);
//	Func2(1, 2);
//	Func2(1, 2, 3);
//
//	return 0;
//}
//
//#include<iostream>
//using namespace std;
////函数重载，函数名相同，参数不同(类型/个数/类型顺序不同)
////返回值没有要求
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//	return left + right;
//}
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}
//
//#include<iostream>
//using namespace std;
////
//int main()
//{
//	int a = 1;
//	int& ra = a;//ra是a的引用，引用也就是别名，a再取一个名称ra
//	int& b = a;
//	int& c = b;
//
//	c = 2;
//
//	return 0;
//}
//
//int main()
//{
//	int a = 1;
//	int& c = a;
//	int d = 2;
//	c = d;//分析：这里是c变成了d的引用？no 还是将d赋值给c？yes
//
//	return 0;
//}

//int main()
//{
//	//int a = 0;
//	//int& b = a;
//
//	const int a = 0;
//	////int& b = a;//b的类型是int，编译不通过，原因：a是只读，b的类型是int，也就是可读可写，所以不行
//	const int& b = a;
//
//	int c = 1;
//	int& d = c;
//	const int& e = c;//行不行？可以，c只是可读可写，e变成别名只读
//
//	//总结：引用取别名时，变量访问的权限可以缩小，不能放大
//
//	int i = 0;
//	double db = i;//隐士类型转换
//	//double& rdb = i;//不行
//	const double& rdb = i;//可以
//
//	return 0;
//}