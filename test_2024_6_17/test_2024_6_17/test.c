//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//二叉树的前序遍历
//给你二叉树的根节点root，返回它节点值的前序遍历。
//int TreeSize(struct TreeNode* root)
//{
//    if (root == NULL)
//        return 0;
//
//    return 1 + TreeSize(root->left) + TreeSize(root->right);
//}
//
//void _preorderTraversal(struct TreeNode* root, int* array, int* pi)
//{
//    if (root == NULL)
//        return;
//
//    array[*pi] = root->val;
//    (*pi)++;
//    _preorderTraversal(root->left, array, pi);
//    _preorderTraversal(root->right, array, pi);
//}
//
//int* preorderTraversal(struct TreeNode* root, int* returnSize) 
//{
//    int size = TreeSize(root);
//    int* array = (int*)malloc(sizeof(int) * size);
//    int i = 0;
//    _preorderTraversal(root, array, &i);
//
//    *returnSize = size;
//    return array;
//}

//单值二叉树
//如果二叉树每个节点都具有相同的值，那么该二叉树就是单值二叉树。
//只有给定的树是单值二叉树时，才返回true；否则返回false。
//bool isUnivalTree(struct TreeNode* root) 
//{
//    if (root == NULL)
//        return true;
//
//    //当前树
//    if (root->left && root->val != root->left->val)
//        return false;
//
//    if (root->right && root->val != root->right->val)
//        return false;
//
//    //递归判断左右子树
//    return isUnivalTree(root->left) && isUnivalTree(root->right);
//}

//二叉树的最大深度
//给定一个二叉树root，返回其最大深度。
//二叉树的 最大深度是指从根节点到最远叶子节点的最长路径上的节点数。
//int maxDepth(struct TreeNode* root) 
//{
//    if (root == NULL)
//        return 0;
//
//    int leftDepth = maxDepth(root->left);
//    int rightDepth = maxDepth(root->right);
//
//    return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
//}

//翻转二叉树
//给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。
//解法1
//struct TreeNode* invertTree(struct TreeNode* root) 
//{
//    if (root == NULL)
//    {
//        return NULL;
//    }
//    else
//    {
//        struct TreeNode* tmp = root->left;
//        root->left = root->right;
//        root->right = tmp;
//
//        invertTree(root->left);
//        invertTree(root->right);
//
//        return root;
//    }
//}

//解法2
//struct TreeNode* invertTree(struct TreeNode* root) 
//{
//    if (root == NULL)
//    {
//        return NULL;
//    }
//    else
//    {
//        struct TreeNode* right = root->right;
//        root->right = invertTree(root->left);
//        root->left = invertTree(right);
//
//        return root;
//    }
//}

//检查两棵树是否相同
//给你两棵二叉树的根节点p和q，编写一个函数来检验这两棵树是否相同。
//如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。
//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    if (p == NULL && q == NULL)
//        return true;
//
//    //结构不同
//    if (p == NULL && q != NULL)
//        return false;
//
//    //结构不同
//    if (p != NULL && q == NULL)
//        return false;
//
//    //值不同
//    if (p->val != q->val)
//        return false;
//
//    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//}

//另一棵树的子树
//给你两棵二叉树root和subRoot。检验root中是否包含和subRoot具有相同结构和节点值的子树。如果存在，返回true；否则，返回false。
//二叉树tree的一棵子树包括tree的某个节点和这个节点的所有后代节点。tree也可以看做它自身的一棵子树。
//bool isSameTree(struct TreeNode* p, struct TreeNode* q) 
//{
//    if (p == NULL && q == NULL)
//        return true;
//
//    //结构不同
//    if (p == NULL && q != NULL)
//        return false;
//
//    //结构不同
//    if (p != NULL && q == NULL)
//        return false;
//
//    //值不同
//    if (p->val != q->val)
//        return false;
//
//    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//}
//
//bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) 
//{
//    if (root == NULL)
//        return false;
//
//    if (isSameTree(root, subRoot))
//        return true;
//
//    return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
//}