#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换

//int main()
//{
//	int num = 10;
//	int ret = ((num & 0xaaaaaaaa) >> 1) + ((num & 0x55555555));
//	printf("%d\n", ret);
//	return 0;
//}
//
//#define SWAP(N) ((N & 0xaaaaaaaa) >> 1) + ((N & 0x55555555) << 1)
//
//int main()
//{
//	int num = 10;
//	int ret = SWAP(num);
//	printf("%d\n", ret);
//	return 0;
//}

//写一个宏，计算结构体中某变量相对于首地址的偏移，并给出说明
//参考offsetof的实现

//#include<stddef.h>
//struct A
//{
//	int a;
//	short b;
//	int c;
//	char d;
//};
//
//int main()
//{
//	printf("%d\n", offsetof(struct A, a));
//	printf("%d\n", offsetof(struct A, b));
//	printf("%d\n", offsetof(struct A, c));
//	printf("%d\n", offsetof(struct A, d));
//	return 0;
//}
//
//struct A
//{
//	int a;
//	short b;
//	int c;
//	char d;
//};
//
//#define OFFSETOF(struct_name, mem_name) (int)&(((struct_name*)0)->mem_name)
//int main()
//{
//	printf("%d\n", OFFSETOF(struct A, a));
//	printf("%d\n", OFFSETOF(struct A, b));
//	printf("%d\n", OFFSETOF(struct A, c));
//	printf("%d\n", OFFSETOF(struct A, d));
//	return 0;
//}