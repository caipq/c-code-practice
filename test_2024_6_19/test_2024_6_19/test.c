//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//二叉树遍历
//编一个程序，读入用户输入的一串先序遍历字符串，根据此字符串建立一个二叉树（以指针方式存储。 
//例如如下的先序遍历字符串： ABC##DE#G##F### 其中“#”表示的是空格，空格字符代表空树。
//建立起此二叉树以后，再对二叉树进行中序遍历，输出遍历结果
//输入描述：
//输入包括1行字符串，长度不超过100。
//输出描述：
//可能有多组测试数据，对于每组数据， 输出将输入字符串建立二叉树后中序遍历的序列，每个字符后面都有一个空格。 每个输出结果占一行。
//typedef struct TreeNode
//{
//    char val;
//    struct TreeNode* left;
//    struct TreeNode* right;
//}TreeNode;
//
//TreeNode* CreateTree(char* str, int* pi)
//{
//    if (str[*pi] == '#')
//    {
//        ++(*pi);
//        return NULL;
//    }
//    else
//    {
//        TreeNode* root = (TreeNode*)malloc(sizeof(TreeNode));
//        root->val = str[*pi];
//        ++(*pi);
//        root->left = CreateTree(str, pi);
//        root->right = CreateTree(str, pi);
//
//        return root;
//    }
//
//}
//
//void InOrder(TreeNode* root)
//{
//    if (root == NULL)
//        return;
//
//    InOrder(root->left);
//    printf("%c ", root->val);
//    InOrder(root->right);
//}
//
//int main()
//{
//    char str[100];
//    scanf("%s", str);
//
//    int i = 0;
//    TreeNode* root = CreateTree(str, &i);
//
//    InOrder(root);
//
//    return 0;
//}