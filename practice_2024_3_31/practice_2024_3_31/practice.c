#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>

//动态内存开辟常见的错误
//1. 对NULL指针的解引用操作
//2. 对动态开辟空间的越界访问
//3. 使用free释放非动态开辟的空间
//4. 使用free释放动态内存的一部分
//5. 对同一块动态开辟空间多次释放
//6. 动态开辟的空间忘记释放(内存泄露) - 比较严重

//int main()
//{
//	int* p = (int*)malloc(1000000000);
//	//对malloc函数的返回值做判断
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	return 0;
//}
//
//int main()
//{
//	int* p = (int*)malloc(10 * sizeof(int));
//	if (p = NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	//越界访问
//	for (i = 0; i < 40; i++)
//	{
//		*(p + i) = i;
//	}
//
//	free(p);
//	p = NULL;
//	return 0;
//}
//
//int main()
//{
//	int arr[10] = { 0 };//栈区
//	int* p = arr;
//	//使用
//
//	free(p);//使用free释放非动态开辟的空间
//	p = NULL;
//
//	return 0;
//}
//
//int main()
//{
//	int* p = malloc(10 * sizeof(int));
//	if (p = NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*p++ = i;
//	}
//	free(p);
//	p = NULL;
//
//	return 0;
//}
//
//int main()
//{
//	int* p = (int*)malloc(100);
//	//使用
//	//释放
//	free(p);
//	p = NULL;//把p置为空值可以避免"多次释放"这种情况发生
//	//再一次使用
//	//释放
//	free(p);
//
//	return 0;
//}
//
//void test()
//{
//	int* p = (int*)malloc(100);
//	if (p = NULL)
//	{
//		return;
//	}
//	//使用
//	//忘记释放
//}
//
//int main()
//{
//	test();
//	//....
//	return 0;
//}
//
//int main()
//{
//	int a = 1;
//	char* p = (char*)&a;
//	if (*p == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}

//#include<string.h>
//void GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(str);
//	strcpy(str, "hello world");
//	printf(str);
//}
//int main()
//{
//	Test();
//	return 0;
//}

//改：
//char* GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//	return p;
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory(str);
//	strcpy(str, "hello world");
//	printf(str);
//	free(str);
//	str = NULL;
//}
//int main()
//{
//	Test();
//	return 0;
//}

//改2
//void GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str);
//	strcpy(str, "hello world");
//	printf(str);
//	free(str);
//	str = NULL;
//}
//int main()
//{
//	Test();
//	return 0;
//}

//GetMemory函数内部创建的数组是在栈区上创建的
//出了函数，p数组的空间就还给了操作系统
//返回的地址是没有实际意义的，如果通过返回的地址去访问内存，就是非法访问
//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//}
//int main()
//{
//	Test();
//	return 0;
//}
//
//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hello");
//	printf(str);
//	//没有free
//}
//int main()
//{
//	Test();
//	return 0;
//}
//
//void Test(void)
//{
//	char* str = (char*)malloc(100);
//	strcpy(str, "hello");
//	free(str);
//	//没有把str置为NULL
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str);
//	}
//}
//int main()
//{
//	Test();
//	return 0;
//}