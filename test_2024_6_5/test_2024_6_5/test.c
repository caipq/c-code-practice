//#define _CRT_SECURE_NO_WARNINGS 1
//
//括号匹配问题
//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串s，判断字符串是否有效。
//有效字符串需满足：
//1.左括号必须用相同类型的右括号闭合。
//2.左括号必须以正确的顺序闭合。
//3.每个右括号都有一个对应的相同类型的左括号。
//
//bool isValid(char* s) {
//    Stack st;
//    StackInit(&st);
//    bool ret;
//    while (*s != '\0')
//    {
//        if (*s == '[' || *s == '(' || *s == '{')
//        {
//            StackPush(&st, *s);
//            ++s;
//        }
//        else
//        {
//            if (StackEmpty(&st))
//            {
//                ret = false;
//                break;
//            }
//            char top = StackTop(&st);
//            if (*s == ']' && top != '[')
//            {
//                ret = false;
//                break;
//            }
//            if (*s == ')' && top != '(')
//            {
//                ret = false;
//                break;
//            }
//            if (*s == '}' && top != '{')
//            {
//                ret = false;
//                break;
//            }
//
//            StackPop(&st);
//            ++s;
//        }
//    }
//
//    if (*s == '\0')
//        ret = StackEmpty(&st);
//
//    StackDestory(&st);
//
//    return ret;
//}


// 用队列实现栈。
//你仅使用两个队列实现一个后入先出（LIFO）的栈，并支持普通栈的全部四种操作（push、top、pop 和 empty）。
//实现MyStack类：
//void push(int x) 将元素x压入栈顶。
//int pop() 移除并返回栈顶元素。
//int top() 返回栈顶元素。
//boolean empty() 如果栈是空的，返回true；否则，返回false。
//
//typedef struct {
//    Queue _q1;
//    Queue _q2;
//} MyStack;
//
//
//MyStack* myStackCreate() {
//    MyStack* st = (MyStack*)malloc(sizeof(MyStack));
//    QueueInit(&st->_q1);
//    QueueInit(&st->_q2);
//
//    return st;
//}
//
//void myStackPush(MyStack* obj, int x) {
//    if (!QueueEmpty(&obj->_q1))
//    {
//        QueuePush(&obj->_q1, x);
//    }
//    else
//    {
//        QueuePush(&obj->_q2, x);
//    }
//}
//
//int myStackPop(MyStack* obj) {
//    Queue* empty = &obj->_q1;
//    Queue* nonEmpty = &obj->_q2;
//    if (QueueEmpty(&obj->_q2))
//    {
//        empty = &obj->_q2;
//        nonEmpty = &obj->_q1;
//    }
//
//    while (QueueSize(nonEmpty) > 1)
//    {
//        QueuePush(empty, QueueFront(nonEmpty));
//        QueuePop(nonEmpty);
//    }
//
//    int top = QueueFront(nonEmpty);
//    QueuePop(nonEmpty);
//    return top;
//}
//
//int myStackTop(MyStack* obj) {
//    if (!QueueEmpty(&obj->_q1))
//    {
//        return QueueBack(&obj->_q1);
//    }
//    else
//    {
//        return QueueBack(&obj->_q2);
//    }
//}
//
//bool myStackEmpty(MyStack* obj) {
//    return QueueEmpty(&obj->_q1) && QueueEmpty(&obj->_q2);
//}
//
//void myStackFree(MyStack* obj) {
//    QueueDestory(&obj->_q1);
//    QueueDestory(&obj->_q2);
//
//    free(obj);
//}