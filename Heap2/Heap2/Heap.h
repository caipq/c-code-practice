//#pragma once
//
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//#include<assert.h>
//
////小堆
//typedef int HPDataType;
//
//typedef struct Heap
//{
//	HPDataType* _a;
//	int _size;
//	int _capacity;
//}Heap;
//
////交换
//void Swap(HPDataType* p1, HPDataType* p2);
//
////初始化
//void HeapInit(Heap* php, HPDataType* a, int n);
//
////向下调整
//void AdjustDown(HPDataType* a, int n, int root);
//
////销毁
//void HeapDestory(Heap* php);
// 
////插入
//void HeapPush(Heap* php, HPDataType x);
//
////删掉堆顶的数据
//void HeapPop(Heap* php);
//
////获取堆顶的数据
//HPDataType HeapTop(Heap* php);