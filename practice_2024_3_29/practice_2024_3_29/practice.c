//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>
//#include<stdlib.h>

//动态内存开辟 - malloc
//
//int main()
//{
//	//假设开辟10个整型的空间 - 10*sizeof(int)
//	//int arr[10];//栈区
//	// 
//	//动态内存开辟
//	int* p = (int*)malloc(10 * sizeof(int));//malloc的默认返回值是void*
//	//使用这些空间的时候
//	if (p == NULL)
//	{
//		//printf("malloc error\n");
//		perror("main");//main: xxxxxxx
//		return 0;
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);//p[i] --> *(p + i)
//	}
//
//	//回收空间
//	free(p);
//	p = NULL;//自己动手把p置为NULL
//
//	return 0;
//}


//calloc函数
//int main()
//{
//	//int* p = (int*)malloc(40);
//	int* p = calloc(10, sizeof(int));
//	if (p == NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}

//realloc函数
//
//int main()
//{
//	//int* p = (int*)malloc(40);
//	int* p = (int*)calloc(10, sizeof(int));
//
//	if (p == NULL)
//	{
//		perror("mian");
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = 5;
//	}
//	//这里需要p指向的空间更大，需要20个int的空间
//	//realloc调整空间
//	int* ptr = realloc(p, 20 * sizeof(int));
//	if (ptr != NULL)
//	{
//		p = ptr;
//	}
//
//	free(p);
//	p = NULL;
//	return 0;
//}
//
//int main()
//{
//	int* p = (int*)realloc(NULL, 40);//这里功能类似于malloc，就是直接在堆区开辟40个字节
//
//	return 0;
//}