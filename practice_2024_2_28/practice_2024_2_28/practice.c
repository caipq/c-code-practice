#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//写一个函数可以交换两个整型变量

//void Swap(int* pa, int* pb)
//{
//	int z = 0;
//	z = *pa;
//	*pa = *pb;
//	*pb = z;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//
//	Swap(&a, &b);
//	
//	printf("a=%d b=%d\n", a, b);
//	return 0;
//}

//写一个函数，实现一个整型有序数组的二分查找

//int binary_search(int a[], int k, int s)
//{
//	int left = 0;
//	int right = s - 1;
//	while (left <= right)
//	{
//		int mid = (left + right) / 2;
//		if (a[mid] > k)
//		{
//			right = mid - 1;
//		}
//		if (a[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int key = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = binary_search(arr, key, sz);
//	if (ret == -1)
//	{
//		printf("找不到\n");
//	}
//	else
//	{
//		printf("找到了，下标是:%d\n",ret);
//	}
//	return 0;
//}

//求第n个斐波那契数（不考虑溢出）
//
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n > 2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//	return 0;
//}

//汉诺塔问题

int MoveNum(int a)
{
	if (a == 1)
	{
		return 1;
	}
	else
	{
		return 2 * MoveNum(a - 1) + 1;
	}
}

void Move(char x, char y)
{
	printf("%c ---> %c\n", x, y);
}

void HNtower(int a, char A, char B, char C)
{
	if (a == 1)
	{
		Move(A, C);
	}
	else
	{
		HNtower(a - 1, A, C, B);
		Move(A, C);
		HNtower(a - 1, B, A, C);
	}
}

int main()
{
	int a = 0;
	printf("请输入你要选择的层数:>");
	scanf("%d", &a);
	int Num = MoveNum(a);
	printf("%d层需要移动%d步\n", a, Num);
	printf("具体的步骤如下");
	HNtower(a, 'A', 'B', 'C');

	return 0;
}