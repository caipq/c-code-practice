//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//字符串左旋
//实现一个函数，可以左旋字符串中的k个字符
//例如；
//ABCD左旋一个字符得到BCDA
//ABCD左旋两个字符得到CDAB

//void string_left_rotate(char* str, int k)
//{
//	int i = 0;
//	int n = strlen(str);
//	for (i = 0; i < k; i++)
//	{
//		//每次左旋一个字符
//		char tmp = *str;//1
//		//2.把后边的n-1个字符往前挪动
//		int j = 0;
//		for (j = 0; j < n - 1; j++)
//		{
//			*(str + j) = *(str + j + 1);
//		}
//		//3.把tmp放在最后
//		*(str + n - 1) = tmp;
//	}
//}
//
//int main()
//{
//	char arr[10] = "ABCDEF";
//	int k = 4;
//	string_left_rotate(arr, k);
//	printf("%s\n", arr);
//	return 0;
//}
//
//#include<assert.h>
//#include<string.h>
//
////三步翻转法
//
//void reverse(char* left, char* right)
//{
//	assert(left);
//	assert(right);
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right;
//	}
//}
//
//void string_left_rotate(char* str, int k)
//{
//	assert(str);
//	int n = strlen(str);
//	reverse(str, str + k - 1);//左
//	reverse(str + k, str + n - 1);//右
//	reverse(str, str + n - 1);//整体
//}
//
//int main()
//{
//	char arr[10] = "ABCDEF";
//	int k = 4;
//	string_left_rotate(arr, k);
//	printf("%s\n", arr);
//	return 0;
//}

//题目名称：
//字符串旋转结果
//题目内容：
// 写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
// 
//例如：给定S1 = AABCD和s2 = BCDAA，返回1
// 给定s1 = abcd和s2 = ACBD，返回0
// 
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC

//#include<string.h>
//
//int is_string_rotate(char* str1, char* str2)
//{
//	int i = 0;
//	int n = strlen(str1);
//	for (i = 0; i < n; i++)
//	{
//		//每次左旋一个字符
//		char tmp = *str1;//1
//		//2.把后边的n-1个字符往前挪动
//		int j = 0;
//		for (j = 0; j < n - 1; j++)
//		{
//			*(str1 + j) = *(str1 + j + 1);
//		}
//		//3.把tmp放在最后
//		*(str1 + n - 1) = tmp;
//
//		if (strcmp(str1, str2) == 0)
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
//
//int main()
//{
//	char arr1[] = "AABCD";
//	char arr2[] = "BCDAA";
//	int ret = is_string_rotate(arr1, arr2);
//	if (ret == 1)
//	{
//		printf("yes\n");
//	}
//	else
//	{
//		printf("no\n");
//	}
//	return 0;
//}

//int main()
//{
//	char arr[20] = "hello ";
//	strcat(arr, "bit");
//	printf("%s\n", arr);
//	return 0;
//}
//
//#include<string.h>
//
//int is_string_rotate(char* str1, char* str2)
//{
//	//长度不相等，肯定表示旋转得到的
//	if (strlen(str1) != strlen(str2))
//	{
//		return 0;
//	}
//	//1.str1字符串后边追加一个str1
//	//AABCDAABCD
//	int len = strlen(str1);
//	strncat(str1, str1, len);
//	//2.判断str2是否为str1的字串
//	char* ret = strstr(str1, str2);
//	//AABCDAABCD
//	//BCDAA
//	return ret != NULL;
//	//if (ret == NULL)
//	//{
//	//	return 0;
//	//}
//	//else
//	//{
//	//	return 1;
//	//}
//}
//
//int main()
//{
//	char arr1[20] = "AABCD";
//	char arr2[] = "BCDAA";
//	int ret = is_string_rotate(arr1, arr2);
//	if (ret == 1)
//	{
//		printf("yes\n");
//	}
//	else
//	{
//		printf("no\n");
//	}
//	return 0;
//}