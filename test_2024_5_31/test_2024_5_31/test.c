//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>

//随机链表的复制
//给定一个链表，每个结点包含一个额外增加的随机指针，该指针可以指向链表中的任何结点或空结点。
//要求返回这个链表的深度拷贝。
//typedef struct Node Node;
//struct Node* copyRandomList(struct Node* head) 
//{
//    if (head == NULL)
//        return NULL;
//
//    //1.拷贝结点，链接到原结点的后面
//    Node* cur = head;
//    while (cur)
//    {
//        Node* copy = (Node*)malloc(sizeof(Node));
//        copy->next = NULL;
//        copy->random = NULL;
//        copy->val = cur->val;
//
//        Node* next = cur->next;
//        cur->next = copy;
//        copy->next = next;
//
//        cur = next;
//    }
//
//    //2.处理拷贝结点的random
//    cur = head;
//    while (cur)
//    {
//        Node* copy = cur->next;
//        if (cur->random)
//            copy->random = cur->random->next;
//        else
//            copy->random = NULL;
//
//        cur = cur->next->next;
//    }
//
//    //3.拆解出复制链表
//    cur = head;
//    Node* copyHead = head->next;
//    while (cur)
//    {
//        Node* copy = cur->next;
//        Node* next = copy->next;
//
//        cur->next = next;
//        if (next)
//            copy->next = next->next;
//        else
//            copy->next = NULL;
//
//        cur = next;
//    }
//
//    return copyHead;
//}

//对链表进行插入排序
//给定单个链表的头head,使用插入排序对链表进行排序，并返回排序后链表的头
//typedef struct ListNode Node;
//struct ListNode* insertionSortList(struct ListNode* head) 
//{
//    if (head == NULL || head->next == NULL)
//        return head;
//
//    Node* sortHead = head;
//    Node* cur = head->next;
//    sortHead->next = NULL;
//
//    while (cur)
//    {
//        Node* next = cur->next;
//
//        //把cur插入到sortHead链表中，并且保持有序
//        if (cur->val <= sortHead->val)
//        {
//            //头插
//            cur->next = sortHead;
//            sortHead = cur;
//        }
//        else
//        {
//            //中间插
//            Node* sortPrev = sortHead;
//            Node* sortCur = sortPrev->next;
//            while (sortCur)
//            {
//                if (cur->val <= sortCur->val)
//                {
//                    sortPrev->next = cur;
//                    cur->next = sortCur;
//                    break;
//                }
//                else
//                {
//                    sortPrev = sortCur;
//                    sortCur = sortCur->next;
//                }
//            }
//
//            //尾插
//            if (sortCur == NULL)
//            {
//                sortPrev->next = cur;
//                cur->next = NULL;
//            }
//        }
//
//        cur = next;
//    }
//
//    return sortHead;
//}