//#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>

//合并两个有序链表
//将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
//typedef struct ListNode Node;
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) 
//{
//    if (list1 == NULL)
//    {
//        return list2;
//    }
//    if (list2 == NULL)
//    {
//        return list1;
//    }
//
//    Node* head = NULL, * tail = NULL;
//    if (list1->val < list2->val)
//    {
//        head = tail = list1;
//        list1 = list1->next;
//    }
//    else
//    {
//        head = tail = list2;
//        list2 = list2->next;
//    }
//
//    //取小的尾插
//    while (list1 && list2)
//    {
//        if (list1->val < list2->val)
//        {
//            tail->next = list1;
//            list1 = list1->next;
//        }
//        else
//        {
//            tail->next = list2;
//            list2 = list2->next;
//        }
//
//        tail = tail->next;
//    }
//
//    if (list1)
//    {
//        tail->next = list1;
//    }
//    else {
//        tail->next = list2;
//    }
//
//    return head;
//}

//优化
//typedef struct ListNode Node;
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) 
//{
//    if (list1 == NULL)
//    {
//        return list2;
//    }
//    if (list2 == NULL)
//    {
//        return list1;
//    }
//    //带哨兵位的头结点，它不存储有效数据，方便尾插
//    Node* head = NULL, * tail = NULL;
//    head = tail = (Node*)malloc(sizeof(Node));
//
//    //取小的尾插
//    while (list1 && list2)
//    {
//        if (list1->val < list2->val)
//        {
//            tail->next = list1;
//            list1 = list1->next;
//        }
//        else
//        {
//            tail->next = list2;
//            list2 = list2->next;
//        }
//
//        tail = tail->next;
//    }
//
//    if (list1)
//    {
//        tail->next = list1;
//    }
//    else {
//        tail->next = list2;
//    }
//
//    Node* realhead = head->next;
//    free(head);
//
//    return realhead;
//}
// 
//给你一个整数数组 nums，其中恰好有两个元素只出现一次，其余所有元素均出现两次。 
//找出只出现一次的那两个元素。你可以按任意顺序返回答案。
//int* singleNumber(int* nums, int numsSize, int* returnSize) 
//{
//    int ret = 0;
//    //将数组中的所有数字异或，出现两次的都异或没了
//    for (int i = 0; i < numsSize; i++)
//    {
//        ret ^= nums[i];
//    }
//
//    //找出ret的第m位为1的位
//    int m = 0;
//    while (m < 32)
//    {
//        if (ret & (1 << m))
//        {
//            break;
//        }
//        else
//        {
//            m++;
//        }
//    }
//
//    //分离
//    int x1 = 0, x2 = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        if (nums[i] & (1 << m))
//        {
//            x1 ^= nums[i];
//        }
//        else
//        {
//            x2 ^= nums[i];
//        }
//    }
//
//    int* retArr = (int*)malloc(sizeof(int) * 2);
//    retArr[0] = x1;
//    retArr[1] = x2;
//
//    *returnSize = 2;
//    return retArr;
//}